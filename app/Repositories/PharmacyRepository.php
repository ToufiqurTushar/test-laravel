<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * Date: 2/3/2018
 * Time: 3:44 PM
 */

namespace App\Repositories;

use App\Models\Pharmacy;
use App\Models\User;
use Illuminate\Http\Request;
use Uzzal\Acl\Models\Role;
use Uzzal\Acl\Models\UserRole;
use Validator;
use DB;

class PharmacyRepository extends AbstractRepository
{
    private static $_ROLE = 'PHARMACY';

    public function __construct(Pharmacy $model)
    {
        $this->_model = $model;
    }

    /**
     * @param Request $request
     * @param bool $isUpdate
     * @param string $except
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(Request $request, $isUpdate = false, $except='')
    {
        if($isUpdate){
            $user_id = ($r = Pharmacy::find($except))?$r->owner_id:0;
            $password = ($request->input('password'))?'required|string|min:6|confirmed':'';
            return Validator::make($request->all(),[
                'name'=>'required|string|max:200',
                'address'=>'required|string|max:200',
                'email' => 'required|string|email|max:200|unique:users,email,' . $user_id . ',user_id',
                'phone'=>'required|string|max:200',
                'password' => $password
            ]);
        }else{
            return Validator::make($request->all(),[
                'name'=>'required|string|max:200',
                'address'=>'required|string|max:200',
                'email' => 'required|string|email|max:200|unique:users',
                'phone'=>'required|string|max:200',
                'password' => 'required|string|min:6|confirmed'
            ]);
        }
    }

    public function create(Request $request)
    {
        $row = null;
        DB::transaction(function () use($request, &$row) {
            $row = $this->_createPharmacy($request);
        });

        return $row;
    }

    public function update($id, Request $request)
    {
        $row = null;
        DB::transaction(function() use($id, $request, &$row){
            $row = $this->_updatePharmacy($id, $request);
        });
        return $row;
    }

    public function _updatePharmacy($id, Request $request){
        $data=[
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
        ];

        $row = $this->_model->find($id);
        if (!empty($row->owner_id)) {
            $this->_updateOwner($row->owner_id, $request);
        }
        return $row->update($data);
    }

    public function _createPharmacy(Request $request)
    {
        $user = $this->_createOwner($request);
        $data=[
            'owner_id' => $user->user_id,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'is_active' => true
        ];

        return $this->_model->create($data);
    }

    private function _createOwner(Request $request){
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'is_active' => true
        ];
        $user = User::create($data);
        if (!empty($user->user_id)) {
            $this->_assignRole($user->user_id);
        }
        return $user;
    }

    private function _updateOwner($id, Request $request){
        $data = $request->all();
        if($request->password){
            $data['password'] = bcrypt($request->password);
        }else{
            unset($data['password']);
        }
        return User::find($id)->update($data);
    }

    private function _assignRole($user_id){
        $role = Role::firstOrCreate(['name'=>self::$_ROLE]);

        UserRole::firstOrCreate([
            'user_id'=>$user_id,
            'role_id'=>$role->role_id
        ]);
    }


}