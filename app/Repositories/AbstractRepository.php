<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 9/22/2017
 * Time: 11:38 AM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

abstract class AbstractRepository implements Repository
{
    /**
     * @var null
     */
    protected $_model = null;

    /**
     * @var null
     */
    protected $primaryKey = null;

    const OPTION_KEY = 'OPTION_KEY';
    const OPTION_VALUE = 'OPTION_VALUE';
    const IS_ACTIVE = 'is_active';

    /**
     * Configuration for html select option values
     *
     * @var array
     */
    protected $optConfig = [];

    /**
     * Setter for $this->optConfig
     *
     * @param array $config
     * @return $this
     */
    protected function setOptConfig(array $config)
    {
        $this->optConfig = $config;
        return $this;
    }

    /**
     * @param Request $request
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(Request $request, $isUpdate = false)
    {
        return Validator::make($request->all(), []);
    }

    /**
     * Activate/Deactivate a specified record in storage
     *
     * @param $id
     * @param $state
     * @return mixed
     */
    public function activate($id, $state)
    {
        $state = $state == 'true' ? true : false;
        return $this->_model->find($id)->update([self::IS_ACTIVE => $state]);
    }

    /**
     * Implement in the inherited class
     *
     * @param Request $request
     */
    public function create(Request $request)
    {
        // TODO: Implement create() method.
    }

    /**
     * Delete a record with specified id
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->_model->find($id)->delete();
    }

    /**
     * Get all records
     *
     * @param bool $active
     * @param bool $pagination
     * @return mixed
     */
    public function getAll($active = true, $pagination = true)
    {
        $records = $this->_model;

        if(!$active == null){
            $records = $records->where(self::IS_ACTIVE, '=', $active);
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->all();
        }
    }

    /**
     * Get the primary key and name-value as html select option
     *
     * @param string $selectedKey
     * @return string
     */
    public function getAllAsOption($selectedKey = '')
    {
        $rows = $this->getAll(true, false);
        $key = $this->optConfig[self::OPTION_KEY];
        $val = $this->optConfig[self::OPTION_VALUE];

        $option = '';
        foreach ($rows as $row){
            $attr = ($row->$key == $selectedKey) ? 'selected' : '';
            $option .= '<option value="'. $row->$key .'" '. $attr .'>'. $row->$val .'</option>';
        }
        return $option;
    }

    /**
     * Retrieve a record with specified id
     *
     * @param $id
     * @return mixed
     */
    public function getItem($id)
    {
        return $this->_model->find($id);
    }

    /**
     * Update a record with specified id
     *
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        return $this->_model->find($id)->update($request->all());
    }

    /**
     * Get the current model name
     *
     * @return string
     */
    public function getModelName()
    {
        return class_basename($this->_model);
    }
}