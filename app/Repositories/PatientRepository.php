<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 10/2/2017
 * Time: 11:59 AM
 */

namespace App\Repositories;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Patient;
use Validator;

class PatientRepository extends AbstractRepository
{
    public function __construct(Patient $model)
    {
        $this->_model = $model;

        if($model){
            $optConfig[self::OPTION_KEY] = $this->primaryKey = $this->_model->getKeyName();
            $optConfig[self::OPTION_VALUE] = 'name';
            $this->setOptConfig($optConfig);
        }
    }

    public function validator(Request $request, $isUpdate = false)
    {
        if($isUpdate){
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'phone' => 'string|max:32|nullable'
                , 'mobile' => 'string|max:32|nullable'
                , 'email' => 'email|max:128|nullable'
                , 'address' => 'required|string|max:256'
                , 'religion' => 'required|string|max:32'
                , 'civil_status' => 'required|string|max:32'
                , 'sex' => 'required|string|max:32'
                , 'dob' => 'date_format:Y-m-d|before:today|nullable'
                , 'birth_place' => 'required|string|max:64'
                , 'blood_group' => 'required|string|max:5'
                , 'national_id' => 'string|max:32|nullable'
                , 'insurance_company' => 'string|max:64|nullable'
                , 'insurance_policy_number' => 'string|max:64|nullable'
//                , 'photo' => 'mimes:jpg,jpeg,bmp,png'
            ]);
        }else{
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'phone' => 'string|max:32|nullable'
                , 'mobile' => 'string|max:32|nullable'
                , 'email' => 'email|max:128|nullable'
                , 'address' => 'required|string|max:256'
                , 'religion' => 'required|string|max:32'
                , 'civil_status' => 'required|string|max:32'
                , 'sex' => 'required|string|max:32'
                , 'dob' => 'date_format:Y-m-d|before:today|nullable'
                , 'birth_place' => 'required|string|max:64|'
                , 'blood_group' => 'required|string|max:5'
                , 'national_id' => 'string|max:32|nullable'
                , 'insurance_company' => 'string|max:64|nullable'
                , 'insurance_policy_number' => 'string|max:64|nullable'
//                , 'photo' => 'mimes:jpg,jpeg,bmp,png'
            ]);
        }
    }

    public function create(Request $request)
    {
        return $this->_model->create([
            'name' => $request->name
            , 'phone' => $request->phone
            , 'mobile' => $request->mobile
            , 'email' => $request->email
            , 'address' => $request->address
            , 'religion' => $request->religion
            , 'civil_status' => $request->civil_status
            , 'sex' => $request->sex
            , 'dob' => $request->dob
            , 'birth_place' => $request->birth_place
            , 'blood_group' => $request->blood_group
            , 'national_id' => $request->national_id
            , 'insurance_company' => $request->insurance_company
            , 'insurance_policy_number' => $request->insurance_policy_number
            , 'photo' => null
            , 'is_active' => true
        ]);
    }

    /**
     * Overrides method of parent class; gets all patients according to search criteria set by user
     *
     * @param bool $pagination
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll($active = true, $pagination = true)
    {
        $active = request('state') == 'deactivated' ? false : true;
        $records = $this->_model->where(Patient::IS_ACTIVE, '=', $active);

        if($n = request(Patient::NAME)){
            $records = $records->where(Patient::NAME,'like','%'.$n.'%');
        }

        if($s = request(Patient::SEX)){
            $records = $records->where(Patient::SEX,'=',$s);
        }

        if($b = request(Patient::BLOOD_GROUP)){
            $records = $records->where(Patient::BLOOD_GROUP,'=',$b);
        }

        if($m = request(Patient::MOBILE)){
            $records = $records->where(Patient::MOBILE,'=',$m);
        }

        if($ni = request(Patient::NATIONAL_ID)){
            $records = $records->where(Patient::NATIONAL_ID,'=',$ni);
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }
}