<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 9/21/2017
 * Time: 9:02 AM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\User;
use Uzzal\Acl\Models\UserRole;
use Validator;
use DB;

class UserRepository extends AbstractRepository
{
    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->_model = $model;
        if($model){
            $config[self::OPTION_KEY] = $this->primaryKey = $this->_model->getKeyName();
            $config[self::OPTION_VALUE] = 'name';
            $this->setOptConfig($config);
        }
    }

    /**
     * @param Request $request
     * @param bool $isUpdate
     * @return mixed
     */
    public function validator(Request $request, $isUpdate = false)
    {
        if($isUpdate){
            return Validator::make($request->all(),[
                'name'=>'required|string|max:255',
                'email'=>'required|string|email|max:255'
            ]);
        }else{
            return Validator::make($request->all(),[
                'name'=>'required|string|max:255',
                'email'=>'required|string|email|max:255',
                'password' => 'required|string|min:6|confirmed'
            ]);
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        DB::beginTransaction();
        try{
            $user = $this->_model->create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'is_active'=>false
            ]);

            $roles = [];
            foreach ($request->input('role_id', []) as $role){
                $roles[] = [
                    'user_id' => $user->user_id,
                    'role_id' => $role
                ];

                UserRole::bulkInsert($roles);
            }
            DB::commit();
        } catch (\Exception $exception){
            DB::rollBack();
        }
        return $user;
    }

    public function update($id, Request $request)
    {
        DB::beginTransaction();
        try{
            $user = $this->_model->find($id)->update($request->all());

            UserRole::where('user_id', $user->user_id)->delete();

            $roles = [];
            foreach ($request->input('role_id', []) as $role){
                $roles[] = [
                    'user_id' => $user->user_id,
                    'role_id' => $role
                ];
            }
            DB::commit();
        } catch (\Exception $exception){
            DB::rollBack();
        }
    }

    /**
     * Gets all users according to search criteria
     *
     * @param bool $pagination
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll($active = true, $pagination = true)
    {
        $active = request('state') == 'deactivated' ? false : true;
        $records = $this->_model->where(User::IS_ACTIVE, '=', $active);

        if($n = request(User::NAME)){
            $records = $records->where(User::NAME, 'like', '%'.$n.'%');
        }

        if($e = request(User::EMAIL)){
            $records = $records->where(User::NAME, 'like', '%'.$e.'%');
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }
}