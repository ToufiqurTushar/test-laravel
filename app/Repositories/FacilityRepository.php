<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 9/28/2017
 * Time: 9:39 PM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Facility;
use Validator;

class FacilityRepository extends AbstractRepository
{
    public function __construct(Facility $model)
    {
        $this->_model = $model;
        if($model){
            $config[self::OPTION_KEY] = $this->primaryKey = $this->_model->getKeyName();
            $config[self::OPTION_VALUE] = 'name';
            $this->setOptConfig($config);
        }
    }

    public function validator(Request $request, $isUpdate = false)
    {
        if($isUpdate){
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'contact_person' => 'required|string|max:128'
                , 'email' => 'email|max:64|nullable'
                , 'mobile' => 'string|max:32|nullable'
                , 'phone' => 'string|max:32|nullable'
                , 'address' => 'string|max:256|nullable'
                , 'specialization' => 'required|string|max:128'
                , 'engagement_date' => 'date_format:Y-m-d|before:today|nullable'
//                , 'photo' => 'mimes:jpg,jpeg,bmp,png'
            ]);
        }else{
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'contact_person' => 'required|string|max:128'
                , 'email' => 'email|max:64|nullable'
                , 'mobile' => 'string|max:32|nullable'
                , 'phone' => 'string|max:32|nullable'
                , 'address' => 'string|max:256|nullable'
                , 'specialization' => 'required|string|max:128'
                , 'engagement_date' => 'date_format:Y-m-d|before:today|nullable'
//                , 'photo' => 'mimes:jpg,jpeg,bmp,png'
            ]);
        }
    }

    public function create(Request $request)
    {
        $this->_model->create([
            'name' => $request->name
            , 'contact_person' => $request->contact_person
            , 'email' => $request->email
            , 'mobile' => $request->mobile
            , 'phone' => $request->phone
            , 'address' => $request->address
            , 'specialization' => $request->specialization
            , 'engagement_date' => $request->engagement_date
            , 'photo' => null
            , 'is_active' => true
        ]);
    }

    /**
     * Gets all facilities according to search criteria
     *
     * @param bool $active
     * @param bool $pagination
     * @return mixed
     */
    public function getAll($active = true, $pagination = true)
    {
        $active = request('state') == 'deactivated' ? false : true;

        $records = $this->_model->where(Facility::IS_ACTIVE, '=', $active);

        if($n = request(Facility::NAME)){
            $records = $records->where(Facility::NAME, 'like', '%' . $n . '%');
        }

        if($m = request(Facility::MOBILE)){
            $records = $records->where(Facility::MOBILE, 'like', '%' . $m . '%');
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }
}