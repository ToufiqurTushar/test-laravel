<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 11/14/2017
 * Time: 12:09 AM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\FacilityUser;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;
use Validator;

class FacilityUserRepository extends AbstractRepository
{
    public function __construct(FacilityUser $model)
    {
        $this->_model = $model;
    }

    public function validator(Request $request, $isUpdate = false)
    {
        return Validator::make($request->all(), [
            'fid' => 'required|integer|max:11',
            'uid' => 'required|integer|max:11'
        ]);
    }

    public function create(Request $request)
    {
        return $this->_model->create([
            'facility_id' => $request->fid,
            'user_id' => $request->uid
        ]);
    }

    public function getAll($active = true, $pagination = true)
    {
        $records = DB::table('facility_users AS fu')
            ->join('facilities AS f', 'fu.facility_id', '=', 'f.facility_id')
            ->join('users AS u', 'fu.user_id', '=', 'u.user_id')
            ->select([
                'fu.facility_user_id'
                , 'fu.facility_id'
                , 'fu.user_id'
                , 'fu.assign_date'
                , 'f.name AS facility_name'
                , 'u.name AS user_name'
                , 'u.email AS user_email'
                , 'u.is_active'
            ]);

        if($fi = request('facility_id')){
            $records = $records->where('fu.facility_id', '=', $fi);
        }

        if($ui = request('user_id')){
            $records = $records->where('fu.user_id', '=', $ui);
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }

    public function getUsers($facility_id = null, $assignable = false, $pagination = true)
    {
        if($assignable){
            $records = DB::table('users as u')
                        ->whereNotIn('u.user_id', function ($query){
                            $query->select(DB::raw("DISTINCT(`fu`.`user_id`) AS `user_id`"))->from('facility_users AS fu');
                        })
                        ->select(['u.user_id', 'u.name', 'u.email', 'u.is_active']);
        } elseif (!$assignable && $facility_id){
            $records = DB::table('facility_users AS fu')
                        ->join('users AS u', 'fu.user_id', '=', 'u.user_id')
                        ->where('fu.facility_id', '=', $facility_id)
                        ->select(['fu.facility_user_id','u.user_id', 'u.name', 'u.email', 'u.is_active', 'fu.assign_date']);
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }

    public function setOptConfig(array $config)
    {
        // TODO: not to implement
    }

    public function getAllAsOption($selectedKey = '')
    {
        // TODO: not to implement
    }

    public function activate($id, $state)
    {
        // TODO: not to implement
    }
}