<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 10/6/2017
 * Time: 10:54 AM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Appointment;
use Validator;
use Illuminate\Support\Facades\DB;

class AppointmentRepository extends AbstractRepository
{
    /**
     * AppointmentRepository constructor.
     * @param Appointment $model
     */
    public function __construct(Appointment $model)
    {
        $this->_model = $model;
        if($model){
            $config[self::OPTION_KEY] = $this->primaryKey = $this->_model->getKeyName();
            $config[self::OPTION_VALUE] = 'patient.name';
            $this->setOptConfig($config);
        }
    }

    /**
     * Validates form data at the time creation or update
     *
     * @param Request $request
     * @param bool $isUpdate
     * @return mixed
     */
    public function validator(Request $request, $isUpdate = false)
    {
        if($isUpdate){
            return Validator::make($request->all(), [
                'patient_id' => 'required|integer|max:11'
                , 'facilitator_id' => 'required|integer|max:11'
                , 'appointment_date' => 'required|date_format:Y-m-d'
                , 'appointment_time' => 'required|date_format:H:i'
                , 'description' => 'string|nullable'
            ]);
        }else{
            return Validator::make($request->all(), [
                'patient_id' => 'required|integer|max:11'
                , 'facilitator_id' => 'required|integer|max:11'
                , 'appointment_date' => 'required|date_format:Y-m-d'
                , 'appointment_time' => 'required|date_format:H:i'
                , 'description' => 'string|nullable'
            ]);
        }
    }

    /**
     * Creates a appointment in the storage
     *
     * @param Request $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        return $this->_model->create([
            'patient_id' => $request->patient_id
            , 'facilitator_id' => $request->facilitator_id
            , 'appointment_date' => $request->appointment_date
            , 'appointment_time' => $request->appointment_time
            , 'description' => $request->description
            , 'is_checked_in' => false
            , 'checked_in_at' => null
        ]);
    }

    /**
     * Cancel/Restores an appointment
     *
     * @param $id
     * @param $state
     * @return mixed
     */
    public function activate($id, $state)
    {
        $state = $state == 'true' ? true : false;
        return $this->_model->find($id)->update(['is_canceled'=>$state, 'canceled_at' => date('Y-m-d H:i:s')]);
    }

    /**
     * Updates the check-in status of an appointment
     *
     * @param $id
     * @param $state
     * @return bool
     */
    public function checkIn($id, $state)
    {
        $state = $state == 'true' ? true : false;
        return $this->_model->find($id)->update(['is_checked_in'=>$state, 'checked_in_at' => date('Y-m-d H:i:s')]);
    }

    /**
     * Get all appointments from storage according to search criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll($active = true, $pagination = true)
    {
        $records = DB::table('appointments as a')
            ->join('patients as p', 'a.patient_id', '=', 'p.patient_id')
            ->join('users as u', 'a.facilitator_id', '=', 'u.user_id')
            ->select(['a.appointment_id'
                , 'a.appointment_date'
                , DB::raw("TIME_FORMAT(`a`.`appointment_time`, '%h:%i %p') AS `appointment_time`")
                , 'a.is_canceled'
                , 'a.canceled_at'
                , 'a.is_checked_in'
                , 'a.checked_in_at'
                , 'a.patient_id'
                , 'p.name as patient_name'
                , 'p.mobile as patient_mobile'
                , 'a.facilitator_id'
                , 'u.name as facilitator_name'
                , 'a.created_at']);

        $is_canceled = false;

        if(request('is_canceled')){ $is_canceled = true; }
        $records = $records->where('a.is_canceled', '=', $is_canceled);

        if(!$active == null){
            $records = $records->where('a.is_checked_in', '=', $active)->orderBy('a.checked_in_at', 'asc');
        }

        if($pn = request('patient_name')){
            $records = $records->where('p.name', 'like', '%' . $pn . '%');
        }

        if($pn = request('patient_mobile')){
            $records = $records->where('p.mobile', 'like', '%' . $pn . '%');
        }

        if($fn = request('facilitator_name')){
            $records = $records->where('u.name', 'like', '%' . $fn . '%');
        }

        if($d = request('appointment_date')){
            $records = $records->where('a.appointment_date', '=', $d);
        }else{
            if(!($pn || $fn || $is_canceled)) {
                $records = $records->where('a.appointment_date', '>=', date('Y-m-d'))
                                    ->orderBy('a.appointment_date', 'asc')
                                    ->orderBy('a.appointment_time', 'asc');
            }else{
                $records = $records->orderBy('a.appointment_date', 'desc')->orderBy('a.appointment_time', 'desc');
            }
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }
}