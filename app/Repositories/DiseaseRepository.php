<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 9/25/2017
 * Time: 4:27 PM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Disease;
use Validator;

class DiseaseRepository extends AbstractRepository
{
    /**
     * DiseaseRepository constructor.
     * @param Disease $model
     */
    public function __construct(Disease $model)
    {
        $this->_model = $model;
        if($model){
            $config[self::OPTION_KEY] = $this->primaryKey = $this->_model->getKeyName();
            $config[self::OPTION_VALUE] = 'name';
            $this->setOptConfig($config);
        }
    }

    /**
     * Validate a record of disease while creating or updating
     *
     * @param Request $request
     * @param bool $isUpdate
     * @return mixed
     */
    public function validator(Request $request, $isUpdate = false)
    {
        if($isUpdate){
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'description' => 'string|nullable'
            ]);
        }else{
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'description' => 'string|nullable'
            ]);
        }
    }

    /**
     * Creates a record of disease
     *
     * @param Request $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        return $this->_model->create([
            'name' => $request->name
            , 'description' => $request->description
            , 'is_active' => true
        ]);
    }

    /**
     * Overrides method of parent class; gets all diseases according to search criteria set by user
     *
     * @param bool $pagination
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll($active = true, $pagination = true)
    {
        $active = request('state') == 'deactivated' ? false : true;
        $records = $this->_model->where(Disease::IS_ACTIVE, '=', $active);

        if($n = request(Disease::NAME)){
            $records = $records->where(Disease::NAME, 'like', '%'. $n .'%');
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }
}