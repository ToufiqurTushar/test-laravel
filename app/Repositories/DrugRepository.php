<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 9/25/2017
 * Time: 4:27 PM
 */

namespace App\Repositories;

use App\Models\Drug;
use Illuminate\Http\Request;
use Validator;

class DrugRepository extends AbstractRepository
{
    public function __construct(Drug $model)
    {
        $this->_model = $model;
        if($model){
            $config[self::OPTION_KEY] = $this->primaryKey = $this->_model->getKeyName();
            $config[self::OPTION_VALUE] = 'name';
            $this->setOptConfig($config);
        }
    }

    public function validator(Request $request, $isUpdate = false)
    {
        if($isUpdate){
            return Validator::make($request->all(),[
                'brand_name' => 'required|string|max:128'
                , 'generic_name' => 'required|string|max:128'
                , 'dosage_form' => 'required|string'
                , 'dosage_unit' => 'required|string'
                , 'age_group' => 'required|string'
                , 'administration_route' => 'required|string'
                , 'description' => 'string|nullable'
                , 'indication' => 'required|string'
//                , 'photo' => 'mimes:jpg,jpeg,bmp,png'
            ]);
        }else{
            return Validator::make($request->all(),[
                'brand_name' => 'required|max:128|string'
                , 'generic_name' => 'required|max:128|string'
                , 'dosage_form' => 'required|string'
                , 'dosage_unit' => 'required|string'
                , 'age_group' => 'required|string'
                , 'administration_route' => 'required|string'
                , 'description' => 'string|nullable'
                , 'indication' => 'required|string'
//                , 'photo' => 'mimes:jpg,jpeg,bmp,png'
            ]);
        }
    }

    public function create(Request $request)
    {
        return $this->_model->create([
            'brand_name' => $request->brand_name
            , 'generic_name' => $request->generic_name
            , 'dosage_form' => $request->dosage_form
            , 'dosage_unit' => $request->dosage_unit
            , 'age_group' => $request->age_group
            , 'administration_route' => $request->administration_route
            , 'description' => $request->description
            , 'indication' => $request->indication
            , 'photo' => null
            , 'is_active' => false
        ]);
    }

    /**
     * Get all drugs according to search criteria
     *
     * @param bool $pagination
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll($active = true, $pagination = true)
    {
        $active = request('state') == 'deactivated' ? false : true;
        $records = $this->_model->where(Drug::IS_ACTIVE, '=', $active);

        if($bn = request(Drug::BRAND_NAME)){
            $records = $records->where(Drug::BRAND_NAME, 'like', '%'. $bn .'%');
        }

        if($gn = request(Drug::GENERIC_NAME)){
            $records = $records->where(Drug::GENERIC_NAME, 'like', '%'. $gn .'%');
        }

        if($df = request(Drug::DOSAGE_FORM)){
            $records = $records->where(Drug::DOSAGE_FORM, '=', $df);
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }

    public function search($str){
        return $this->_model->where(function($qr) use($str){
                                $str = '%'.$str.'%';
                                $qr->where('brand_name','LIKE', $str)
                                    ->orWhere('generic_name','LIKE', $str);
                            })
                            ->where('is_active', true)
                            ->get();
    }
}