<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * Date: 12/19/2017
 * Time: 1:25 AM
 */

namespace App\Repositories;


use App\Models\ClinicalManagement;
use Auth;
use Illuminate\Http\Request;

class ClinicalManagementRepository extends AbstractRepository
{
    public function __construct(ClinicalManagement $model)
    {
        $this->_model = $model;
    }

    public function create(Request $request)
    {
        $data =  $request->all();
        if(!array_key_exists('patient_id', $data) || $data['patient_id']<=0){
            return false;
        }

        $today = date('Y-m-d', strtotime('now'));
        $data['doctor_id'] = Auth::id();
        $data['visit_date'] = $today;
        $data['patient_id'] = $request->get('patient_id');//todo need to work on this.
        $data['past_illness'] = json_encode($request->get('past_illness'));
        $data['personal'] = json_encode($request->get('personal'));
        $data['routine'] = json_encode($request->get('routine'));
        $data['meds'] = json_encode($request->get('meds'));
        $data['tests'] = json_encode($request->get('tests'));

        $data = array_merge($request->all(), $data);
        $row = $this->_model->where('patient_id', $data['patient_id'])
                    ->where('visit_date', $today)->first();

        if($row){
            return $row->update($data);
        }else{
            return $this->_model->create($data);
        }
    }

    public function getVisitHistoryDate($patient_id){
        return $this->_model->where('patient_id', $patient_id)->get(['visit_date']);
    }

    public function getRow($patient_id, $date){
        return $this->_model->where('patient_id', $patient_id)
            ->where('visit_date', $date)->first();
    }


}