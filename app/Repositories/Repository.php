<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 9/22/2017
 * Time: 11:31 AM
 */

namespace App\Repositories;

use Illuminate\Http\Request;

interface Repository
{
    public function validator(Request $request, $isUpdate=false);
    function getAll($active = true, $pagination = true);
    function getItem($id);
    function create(Request $request);
    function update($id, Request $request);
    function destroy($id);
    function activate($id, $state);
}