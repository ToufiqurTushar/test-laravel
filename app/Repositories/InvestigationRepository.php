<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 10/4/2017
 * Time: 11:05 AM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Investigation;
use Validator;

class InvestigationRepository extends AbstractRepository
{
    public function __construct(Investigation $model)
    {
        $this->_model = $model;
        if($model){
            $config[self::OPTION_KEY] = $this->primaryKey = $this->_model->getKeyName();
            $config[self::OPTION_VALUE] = 'name';
            $this->setOptConfig($config);
        }
    }

    public function validator(Request $request, $isUpdate = false)
    {
        if($isUpdate){
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'description' => 'string|nullable'
                , 'params' => 'string|nullable'
            ]);
        }else{
            return Validator::make($request->all(),[
                'name' => 'required|string|max:128'
                , 'description' => 'string|nullable'
                , 'params' => 'string|nullable'
            ]);
        }
    }

    public function create(Request $request)
    {
        return $this->_model->create([
            'name' => $request->name
            , 'description' => $request->description
            , 'params' => $request->params
            , 'is_active' => true
        ]);
    }

    public function getAll($active = true, $pagination = true)
    {
        $active = request('state') == 'deactivated' ? false : true;
        $records = $this->_model->where(Investigation::IS_ACTIVE, '=', $active);

        if($n = request(Investigation::NAME)){
            $records = $records->where(Investigation::NAME, 'like', '%'. $n .'%');
        }

        if($pagination){
            return $records->paginate(25);
        }else{
            return $records->get();
        }
    }
}