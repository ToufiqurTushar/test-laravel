<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * @date: 4/29/2018 10:22 PM
 */

namespace App\Services;


use App\Models\Pharmacy;
use App\Models\TestResult;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestResultService
{
    /**
     * @param Request $req
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(Request $req){
        return Validator::make($req->all(),[
            'patient_id'=>'required',
            'owner_id'=>'required',
            'type'=>'required|in:BLOOD_PRESSURE,BLOOD_SUGAR',
            'date'=>'required',
            'result'=>'required'
        ]);
    }
    public function insert(Request $req){
        $data = $req->all();
        $row = Pharmacy::where('owner_id', $req->owner_id)->first();
        if($row){
            $data['pharmacy_id'] = $row->pharmacy_id;
        }
        return TestResult::create($data);
    }

    public function getAgentReport($all=false){
        $owner = Auth::id();
        $row = Pharmacy::where('owner_id', $owner)->first();
        if(!$row){
            return [];
        }

        if($all){
            $sql = "SELECT DATE_FORMAT(tr.`date`, '%Y-%m') AS `month`, tr.`type`, COUNT(tr.`test_result_id`) AS total FROM test_results AS tr
                WHERE tr.`pharmacy_id`={$row->pharmacy_id}
                GROUP BY DATE_FORMAT(tr.`date`, '%Y-%m'), tr.type";
        }else{
            $date = date('Y-m', strtotime('now'));
            $sql = "SELECT DATE_FORMAT(tr.`date`, '%Y-%m') AS `month`, tr.`type`, COUNT(tr.`test_result_id`) AS total FROM test_results AS tr
                WHERE tr.`pharmacy_id`={$row->pharmacy_id} AND DATE_FORMAT(tr.`date`, '%Y-%m')='{$date}' 
                GROUP BY DATE_FORMAT(tr.`date`, '%Y-%m'), tr.type";
        }

        return DB::select($sql);
    }

    public function getTestResult($patient_id){
        $rows = TestResult::where('patient_id', $patient_id)
                ->select(['type', 'result', 'date'])
                ->orderBy('date', 'desc')
                ->get()
                ->map(function($v){
                    $v->result = json_decode($v->result);
                    return $v;
                });

        return $rows;
    }
}