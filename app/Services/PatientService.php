<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * @date: 3/30/2018 11:45 PM
 */

namespace App\Services;


use App\Models\Patient;
use Carbon\Carbon;

class PatientService
{
    const REPEAT_WEEK = 'week';
    const REPEAT_MONTH = 'month';
    const REPEAT_15DAYS = '15days';

    /**
     * @param $q
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function search($q){
        //dd(self::repeat(TestEnum::BLOOD_SUGAR, '2018-01-01', '2018-02-28', true));

        return Patient::where('phone', $q)
            ->orWhere('mobile', $q)
            ->orWhere('email', $q)
            ->select(['patient_id', 'name', 'address', 'phone', 'mobile', 'sex', 'dob', 'blood_group'])
            ->with('clinical')
            ->get()
            ->map(function($v){
                $v->clinical = self::mapClinical($v);
                return $v;
            })
            ->map(function($v){
                $v->test_dates = self::mergeTestsDates($v);
                return $v;
            });
    }

    private static function mergeTestsDates($v){
        $list = collect([]);
        foreach($v->clinical as $v){
            $list->push($v->test_dates);
        }

        $list = $list->reduce(function($c, $i){
           if($c){
               return array_merge($c, $i);
           }
           return $i;
        });
        return $list;
    }

    private static function mapClinical($v){
        return $v->clinical->map(function($v){
            $r = json_decode($v->tests);
            $dts = [];
            $r=($r)?$r:[];
            foreach ($r as $val){
                $dts[] = self::repeat($v->visit_date, $val->repeat_until, $val->test_type, $val->is_repeat);
            }
            $v->test_dates = array_flatten($dts);
            return $v;
        });
    }

    private static function repeat($start, $end, $type, $is_repeat=false, $duration='week'){
        $curr = $start;
        $start = strtotime($start);
        $end = strtotime($end);
        $date_list = [];

        $dt = Carbon::createFromTimestamp($start);
        if($is_repeat){
            while ($end >= strtotime($curr)){
                $date_list[] = date('Y-m-d', strtotime($curr)).'_'.$type;

                switch($duration){
                    case self::REPEAT_15DAYS:
                        $curr = $dt->addDays(15);
                        break;
                    case self::REPEAT_MONTH:
                        $curr = $dt->addMonth(1);
                        break;
                    default:
                        $curr = $dt->addWeek(1);
                }
            }
        }else{
            $date_list[] = date('Y-m-d', strtotime($curr)).'_'.$type;
        }

        return $date_list;
    }

    public function find($id){
        return Patient::find($id);
    }
}