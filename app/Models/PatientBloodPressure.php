<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientBloodPressure extends Model
{
    /**
     * @var string
     */
    protected $table = 'patient_blood_pressure';

    /**
     * @var string
     */
    protected $primaryKey = 'patient_blood_pressure_id';

    /**
     * Fillable data field
     */
    const PATIENT_ID = 'patient_id';
    const SYSTOLIC = 'systolic';
    const DIASTOLIC = 'diastolic';
    const PULSE = 'pulse';
    const DATE = 'date';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @var array
     */
    protected $fillable = [
        self::PATIENT_ID,
        self::SYSTOLIC,
        self::DIASTOLIC,
        self::PULSE,
        self::DATE
    ];

    /**
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT
    ];
    /**
     * Get the patient who owns the blood pressure
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient', 'patient_id', 'patient_id');
    }

}
