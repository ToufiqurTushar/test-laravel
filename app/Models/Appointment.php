<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * @var string
     */
    protected $table = 'appointments';

    /**
     * @var string
     */
    protected $primaryKey = 'appointment_id';

    /**
     * Fillable data field
     */
    const PATIENT_ID = 'patient_id';
    const FACILITATOR_ID = 'facilitator_id';
    const APPOINTMENT_DATE = 'appointment_date';
    const APPOINTMENT_TIME = 'appointment_time';
    const DESCRIPTION = 'description';
    const IS_CHECKED_IN = 'is_checked_in';
    const CHECKED_IN_AT = 'checked_in_at';
    const IS_CANCELED = 'is_canceled';
    const CANCELED_AT = 'canceled_at';

    /**
     * @var array
     */
    protected $fillable = [
        self::PATIENT_ID
        , self::FACILITATOR_ID
        , self::APPOINTMENT_DATE
        , self::APPOINTMENT_TIME
        , self::DESCRIPTION
        , self::IS_CHECKED_IN
        , self::CHECKED_IN_AT
        , self::IS_CANCELED
        , self::CANCELED_AT
    ];

    /**
     * Get the patient who owns the appointment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient', 'patient_id', 'patient_id');
    }

    /**
     * Get the facilitator who facilitate the appointment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function facilitator()
    {
        return $this->belongsTo('App\Models\User', 'facilitator_id', 'user_id');
    }
}
