<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacilityUser extends Model
{
    /**
     * @var string
     */
    protected $table = 'facility_users';

    /**
     * @var null
     */
    protected $primaryKey = 'facility_user_id';

    /**
     * Fillable data fields
     */
    const FACILITY_ID = 'facility_id';
    const USER_ID = 'user_id';

    /**
     * @var array
     */
    protected $fillable = [
        self::FACILITY_ID,
        self::USER_ID
    ];

    public function facility(){
        return $this->hasOne(Facility::class, 'facility_id', 'facility_id');
    }
}
