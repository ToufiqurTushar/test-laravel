<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestResult extends Model
{
    protected $primaryKey = 'test_result_id';

    protected $fillable=[
        'patient_id',
        'pharmacy_id',
        'type',
        'date',
        'result'
    ];
}
