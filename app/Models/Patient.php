<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * @var string
     */
    protected $table = 'patients';

    /**
     * @var string
     */
    protected $primaryKey = 'patient_id';

    /**
     * Fillable data fields
     */
    const NAME = 'name';
    const PHONE = 'phone';
    const MOBILE = 'mobile';
    const EMAIL = 'email';
    const ADDRESS = 'address';
    const RELIGION = 'religion';
    const CIVIL_STATUS = 'civil_status';
    const SEX = 'sex';
    const DOB = 'dob';
    const BIRTH_PLACE = 'birth_place';
    const BLOOD_GROUP = 'blood_group';
    const NATIONAL_ID = 'national_id';
    const INSURANCE_COMPANY = 'insurance_company';
    const INSURANCE_POLICY_NUMBER = 'insurance_policy_number';
    const PHOTO = 'photo';
    const IS_ACTIVE = 'is_active';

    /**
     * These fields are allowed to be filled by the user explicitly
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::PHONE,
        self::MOBILE,
        self::EMAIL,
        self::ADDRESS,
        self::RELIGION,
        self::CIVIL_STATUS,
        self::SEX,
        self::DOB,
        self::BIRTH_PLACE,
        self::BLOOD_GROUP,
        self::NATIONAL_ID,
        self::INSURANCE_COMPANY,
        self::INSURANCE_POLICY_NUMBER,
        self::PHOTO,
        self::IS_ACTIVE
    ];

    /**
     * Get the appointments those are owned by this patient
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointment()
    {
        return $this->hasMany(Appointment::class, 'patient_id', 'patient_id');
    }

    public function clinical(){
        return $this->hasMany(ClinicalManagement::class, 'patient_id', 'patient_id')
            ->select(['patient_id','visit_date', 'tests'])->orderBy('visit_date', 'desc');
    }

    public function bloodPressures(){
        return $this->hasMany(PatientBloodPressure::class, 'patient_id', 'patient_id')
            ->orderBy('date', 'desc');
    }
}
