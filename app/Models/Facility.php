<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    /**
     * @var string
     */
    protected $table = 'facilities';

    /**
     * @var string
     */
    protected $primaryKey = 'facility_id';

    /**
     * Fillable data fields
     */
    const NAME = 'name';
    const CONTACT_PERSON = 'contact_person';
    const EMAIL = 'email';
    const MOBILE = 'mobile';
    const PHONE = 'phone';
    const ADDRESS = 'address';
    const SPECIALIZATION = 'specialization';
    const ENGAGEMENT_DATE = 'engagement_date';
    const PHOTO = 'photo';
    const IS_ACTIVE = 'is_active';

    /**
     * @var array
     */
    protected $fillable = [
        self::NAME
        , self::CONTACT_PERSON
        , self::EMAIL
        , self::MOBILE
        , self::PHONE
        , self::ADDRESS
        , self::SPECIALIZATION
        , self::ENGAGEMENT_DATE
        , self::PHOTO
        , self::IS_ACTIVE
    ];
}
