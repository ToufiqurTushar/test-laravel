<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'user_id';

    /**
     * Fillable data fields
     */
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const IS_ACTIVE = 'is_active';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME, self::EMAIL, self::PASSWORD, self::IS_ACTIVE
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles() {
        return $this->hasMany('Uzzal\Acl\Models\UserRole', 'user_id', 'user_id');
    }

    /**
     * Get te appointments those are owned by this user/facilitator
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointment()
    {
        return $this->hasMany('App\Models\Appointment', 'facilitator_id', 'user_id');
    }

    public function userFacility(){
        return $this->hasOne(FacilityUser::class, 'user_id', 'user_id');
    }
}
