<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pharmacy extends Model
{
    protected $primaryKey = 'pharmacy_id';

    protected $fillable=[
        'owner_id'
        ,'name'
        ,'address'
        ,'phone'
        ,'is_active'
    ];

    public function user(){
        return $this->hasOne(User::class, 'user_id','owner_id');
    }
}
