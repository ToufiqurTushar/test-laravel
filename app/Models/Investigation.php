<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Investigation extends Model
{
    /**
     * @var string
     */
    protected $table = 'investigations';

    /**
     * @var string
     */
    protected $primaryKey = 'investigation_id';

    /**
     * Fillable data fields
     */
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const PARAMS = 'params';
    const IS_ACTIVE = 'is_active';

    /**
     * @var array
     */
    protected $fillable = [
        self::NAME
        , self::DESCRIPTION
        , self::PARAMS
        , self::IS_ACTIVE
    ];
}
