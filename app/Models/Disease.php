<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    /**
     * @var string
     */
    protected $table = 'diseases';

    /**
     * @var string
     */
    protected $primaryKey = 'disease_id';

    /**
     * Fillable data fields
     */
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const IS_ACTIVE = 'is_active';

    /**
     * @var array
     */
    protected $fillable = [
        self::NAME
        , self::DESCRIPTION
        , self::IS_ACTIVE
    ];
}
