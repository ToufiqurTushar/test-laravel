<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClinicalManagement extends Model
{
    /**
     * @var string
     */
    protected $table = 'clinical_managements';

    /**
     * @var string
     */
    protected $primaryKey = 'clinical_management_id';

    protected $fillable=[
        'visit_date',
        'patient_id',
        'doctor_id',
        'past_illness',
        'routine',
        'present_complaints',
        'medical_history',
        'medication',
        'personal',
        'family',
        'social',
        'allergy',
        'psychiatric_illness',
        'appearance',
        'anaemia',
        'blood_pressure',
        'venous_pressure',
        'jaundice',
        'temperature',
        'clubbing',
        'cyanosis',
        'pulse',
        'oedema',
        'height',
        'weight',
        'other_examination',
        'cardiac_vascular_system',
        'respiratory_system',
        'elementary_system',
        'musculoskeletal_system',
        'investigation_specific',
        'diagnosis',
        'diagnosis_remarks',
        'meds',
        'tests',
        'medication_advice',
        'menstruation',
        'menstruation_type',
        'para',
        'gravida',
        'menarche',
        'menopause'
    ];

}
