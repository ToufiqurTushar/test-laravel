<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Drug extends Model
{
    /**
     * @var string
     */
    protected $table = 'drugs';

    /**
     * @var string
     */
    protected $primaryKey = 'drug_id';

    /**
     * Fillable data fields
     */
    const BRAND_NAME = 'brand_name';
    const GENERIC_NAME = 'generic_name';
    const DOSAGE_FORM = 'dosage_form';
    const DOSAGE_UNIT = 'dosage_unit';
    const AGE_GROUP = 'age_group';
    const ADMINISTRATION_ROUTE = 'administration_route';
    const DESCRIPTION = 'description';
    const INDICATION = 'indication';
    const PHOTO = 'photo';
    const IS_ACTIVE = 'is_active';

    /**
     * @var array
     */
    protected $fillable = [
        self::BRAND_NAME
        , self::GENERIC_NAME
        , self::DOSAGE_FORM
        , self::DOSAGE_UNIT
        , self::AGE_GROUP
        , self::ADMINISTRATION_ROUTE
        , self::DESCRIPTION
        , self::INDICATION
        , self::PHOTO
        , self::IS_ACTIVE
    ];
}
