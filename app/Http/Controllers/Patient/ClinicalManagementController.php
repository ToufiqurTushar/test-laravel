<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * Date: 12/15/2017
 * Time: 12:09 PM
 */

namespace App\Http\Controllers\Patient;


use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Patient;
use App\Models\PatientBloodPressure;
use App\Repositories\ClinicalManagementRepository;
use Illuminate\Http\Request;
use Log;

class ClinicalManagementController extends Controller
{
    public function index(){
        return view('patient.clinical-management.index');
    }

    public function manage($appointment_id){
        $row = Appointment::find($appointment_id);
        $patient = $row->patient;
        return view('patient.clinical-management.manage',[
            'patient' => $patient
        ]);
    }

    public function history($patient_id){
        $patient = Patient::find($patient_id);
        return view('patient.clinical-management.history',[
            'patient' => $patient
        ]);
    }

    public function store(Request $req, ClinicalManagementRepository $repo){
        if($repo->create($req)){
            return response()->json([
                'status'=>'success',
                'msg'=>'Data saved successfully!'
            ]);
        }else{
            return response()->json([
                'status'=>'failed',
                'msg'=>'Something went terribly wrong!'
            ]);
        }
    }

    public function visitDates($patient_id, ClinicalManagementRepository $repo){
        return $repo->getVisitHistoryDate($patient_id);
    }

    public function visit($patient_id, $date, ClinicalManagementRepository $repo){
        $row = $repo->getRow($patient_id, $date);
        $blood_pressures = PatientBloodPressure::where(PatientBloodPressure::PATIENT_ID, $patient_id)
            ->orderBy('date', 'desc')
            ->get();

        if(!$row){
            return ['status'=>'failed'];
        }
        $row->meds = json_decode($row->meds);
        $row->tests = json_decode($row->tests);
        $row->past_illness = json_decode($row->past_illness);
        $row->personal = json_decode($row->personal);
        $row->routine = json_decode($row->routine);
        $row->blood_pressures = json_decode($blood_pressures);
        return $row;
    }
}