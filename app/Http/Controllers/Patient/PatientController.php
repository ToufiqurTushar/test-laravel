<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\AppController;
use App\Repositories\PatientRepository;
use App\Services\TestResultService;
use Illuminate\Http\Request;

class PatientController extends AppController
{
    public function __construct(PatientRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Display a listing of the registered patients.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('patient.index', [
            'rows' => $this->_repository->getAll()
        ]);
    }

    /**
     * Show the form for registering a new patient.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patient.create');
    }

    /**
     * Store a newly created patient in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        parent::store($request);
        return redirect('patient')->with('msg', $this->_repository->getModelName().' registered successfully!');
    }

    /**
     * Display the specified patient.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('patient.show',[
            'row' => $this->_repository->getItem($id)
        ]);
    }

    /**
     * Show the form for editing the specified patient.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('patient.edit',[
            'row' => $this->_repository->getItem($id)
        ]);
    }

    /**
     * Update the specified patient in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        parent::update($request, $id);
        return redirect('patient')->with('msg', $this->_repository->getModelName().' with id: '.$id.' updated successfully!');
    }

    public function testResult($patient_id, TestResultService $service){
        return $service->getTestResult($patient_id);
    }
}
