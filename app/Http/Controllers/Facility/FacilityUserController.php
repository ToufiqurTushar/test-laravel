<?php

namespace App\Http\Controllers\Facility;

use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
use App\Repositories\FacilityUserRepository;

class FacilityUserController extends AppController
{
    public function __construct(FacilityUserRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return view('facility.user.index',[
            'rows' => $this->_repository->getUsers($id),
            'facility_id' => $id
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('facility.user.add',[
            'rows' => $this->_repository->getUsers(null, true),
            'facility_id' => $id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        parent::store($request);
        return redirect('facility/'.$request->fid.'/user/add')->with('msg','Access permission granted!');
    }
}
