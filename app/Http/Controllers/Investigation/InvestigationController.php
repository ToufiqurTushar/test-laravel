<?php

namespace App\Http\Controllers\Investigation;

use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
use App\Repositories\InvestigationRepository;
use PhpParser\Node\Scalar\String_;

class InvestigationController extends AppController
{
    public function __construct(InvestigationRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('investigation.index',[
            'rows' => $this->_repository->getAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('investigation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        parent::store($request);
        return redirect('investigation')->with('msg', $this->_repository->getModelName().' added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = $this->_repository->getItem($id);
        $row->params = str_to_array($row->params);
        return view('investigation.show',[
            'row' => $row
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('investigation.edit',[
            'row' => $this->_repository->getItem($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        parent::update($request, $id);
        return redirect('investigation')->with('msg', $this->_repository->getModelName().' with id: '.$id.' updated successfully!');
    }
}
