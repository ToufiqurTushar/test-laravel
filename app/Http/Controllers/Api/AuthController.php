<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * @date: 3/30/2018 1:47 AM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Uzzal\ApiToken\Authenticate;

class AuthController extends Controller
{
    use Authenticate;
}