<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * @date: 3/30/2018 11:44 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Services\PatientService;

class PatientController extends Controller
{
    public function index(PatientService $service){
        $q = request('q');
        if($q){
            return $service->search($q);
        }
        return [];
    }

    public function show($id, PatientService $service){
        return $service->find($id);
    }
}