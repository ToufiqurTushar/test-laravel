<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * Date: 2/4/2018
 * Time: 12:02 AM
 */

namespace App\Http\Controllers\Api;


use App\Enum\TestEnum;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function typeList()
    {
        return response()->json(TestEnum::asArray());
    }
}