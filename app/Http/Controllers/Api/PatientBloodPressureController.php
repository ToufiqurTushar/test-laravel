<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * @date: 3/30/2018 11:44 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\PatientBloodPressure;
use App\Services\PatientService;
use Illuminate\Http\Request;

class PatientBloodPressureController extends Controller
{
    public function index(Request $request){
        $blood_pressures = PatientBloodPressure::where(PatientBloodPressure::PATIENT_ID, $request->patient_id)
            ->orderBy('date', 'desc')
            ->get();

        return response()->json($blood_pressures);
    }

    public function store(Request $request){

        $response  = PatientBloodPressure::create($request->only([
            PatientBloodPressure::PATIENT_ID,
            PatientBloodPressure::SYSTOLIC,
            PatientBloodPressure::DIASTOLIC,
            PatientBloodPressure::PULSE,
            PatientBloodPressure::DATE
        ]));


        return response()->json($response);
    }
}