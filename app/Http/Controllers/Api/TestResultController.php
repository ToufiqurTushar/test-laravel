<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * @date: 4/29/2018 10:21 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Services\TestResultService;
use Illuminate\Http\Request;

class TestResultController extends Controller
{
    public function index(TestResultService $service){
        return $service->getAgentReport(request('all'));
    }

    public function store(Request $request, TestResultService $service){
        $validator = $service->validator($request);
        if($validator->fails()){
            return ['status'=>'failed', 'errors'=> $validator->getMessageBag()];
        }

        $resp = $service->insert($request);
        if($resp){
            return ['status'=>'success', 'remote_id'=>$resp->test_result_id];
        }else{
            return ['status'=>'failed'];
        }
    }

}