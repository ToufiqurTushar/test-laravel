<?php

namespace App\Http\Controllers\Disease;

use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
use App\Repositories\DiseaseRepository;

class DiseaseController extends AppController
{
    public function __construct(DiseaseRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('disease.index',[
            'rows' => $this->_repository->getAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('disease.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        parent::store($request);
        return redirect('disease')->with('msg', $this->_repository->getModelName().' added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('disease.show',[
            'row' => $this->_repository->getItem($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('disease.edit',[
            'row' => $this->_repository->getItem($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        parent::update($request, $id);
        return redirect('disease')->with('msg', $this->_repository->getModelName().' with id: '.$id.' updated successfully!');
    }
}
