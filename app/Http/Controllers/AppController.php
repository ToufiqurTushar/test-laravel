<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

abstract class AppController extends Controller
{
    protected $_repository = null;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->_repository->validator($request)->validate();
        $this->_repository->create($request);
    }

    /**
     * Activates/Deactivates a specified resource
     * @param $id
     * @param $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($id, $state)
    {
        $this->_repository->activate($id, $state);
        $status = $state == 'true' ? 'activated' : 'de-activated';
        return redirect()->back()->with('msg', $this->_repository->getModelName().' with id: '.$id. ' ' .$status.' successfully!');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->_repository->validator($request, true)->validate();
        $this->_repository->update($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->_repository->destroy($id);
        return redirect()->back()->with('msg', $this->_repository->getModelName().' with id: '.$id.' removed successfully!');
    }
}
