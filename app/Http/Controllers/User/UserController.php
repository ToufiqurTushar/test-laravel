<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Uzzal\Acl\Models\Role;

class UserController extends AppController
{
    public function __construct(UserRepository $repository)
    {
        $this->_repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.user.index',[
            'rows' => $this->_repository->getAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.user.create',[
            'roles' => Role::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        parent::store($request);
        return redirect('user')->with('msg', $this->_repository->getModelName().' registered successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.user.edit',[
            'row'=>$this->_repository->getItem($id),
            'roles' => Role::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        parent::update($request, $id);
        return redirect('user')->with('msg', $this->_repository->getModelName().' with id: '.$id.' updated successfully!');
    }
}
