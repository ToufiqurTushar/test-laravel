<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * Date: 2/3/2018
 * Time: 3:34 PM
 */

namespace App\Http\Controllers\Pharmacy;


use App\Http\Controllers\AppController;
use App\Repositories\PharmacyRepository;
use Illuminate\Http\Request;

class PharmacyController extends AppController
{
    public function __construct(PharmacyRepository $repo)
    {
        $this->_repository = $repo;
    }

    public function index()
    {
        return view('pharmacy.pharmacy.index',[
            'rows' => $this->_repository->getAll()
        ]);
    }

    public function create()
    {
        return view('pharmacy.pharmacy.create');
    }

    public function store(Request $request)
    {
        parent::store($request);
        return redirect('pharmacy')->with('msg', $this->_repository->getModelName().' added successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pharmacy.pharmacy.edit', [
            'row' => $this->_repository->getItem($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->_repository->validator($request, true, $id)->validate();
        $this->_repository->update($id, $request);
        return redirect('pharmacy')->with('msg', $this->_repository->getModelName().' with id: '.$id.' updated successfully!');
    }


}