<?php

namespace App\Http\Controllers\Appointment;

use App\Http\Controllers\AppController;
use Illuminate\Http\Request;
use App\Repositories\AppointmentRepository;

class AppointmentController extends AppController
{
    /**
     * AppointmentController constructor.
     * @param AppointmentRepository $repository
     */
    public function __construct(AppointmentRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('appointment.index', [
            'rows' => $this->_repository->getAll(null)
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkedIn()
    {
        return view('appointment.checked-in', [
            'rows' => $this->_repository->getAll(true)
        ]);
    }

    public function canceled()
    {
        dd('in canceled controller!');
    }

    /**
     * Show the form for creating a new appointment
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $patient_id = request()->pid ? request()->pid : '';
        return view('appointment.create',['patient_id' => $patient_id]);
    }

    /**
     * Store a newly created appointment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        parent::store($request);
        return redirect('appointment')->with('msg', 'An '.$this->_repository->getModelName().' has been made successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('appointment.edit', [
            'rows' => $this->_repository->getItem($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        parent::update($request, $id);
        return redirect('appointment')->with('msg', $this->_repository->getModelName().' id: '. $id .' has been updated successfully!');
    }

    /**
     * Update the cancellation status of an appointment
     *
     * @param $id
     * @param $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($id, $state)
    {
        $this->_repository->activate($id, $state);
        $status = $state == 'true' ? 'canceled' : 'restored';
        return redirect()->back()->with('msg', $this->_repository->getModelName().' with id: '.$id. ' is ' .$status.' successfully!');
    }

    /**
     * Update the check-in status of an appointment
     *
     * @param $id
     * @param $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkIn($id, $state)
    {
        $this->_repository->checkIn($id, $state);
        $status = $state == 'true' ? 'checked in' : '';
        return redirect()->back()->with('msg', $this->_repository->getModelName().' with id: '.$id. ' is ' .$status.' successfully!');
    }

    /**
     * Shows existing appointments of a service provider or a patient
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAppointmentTimes()
    {
        $view = request('patient_id') ? 'shared.patient-time' : 'shared.facilitator-time';

        return view($view, [
            'rows' => $this->_repository->getAll(null, false)
        ]);
    }
}
