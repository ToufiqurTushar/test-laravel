<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 * Date: 2/3/2018
 * Time: 11:56 PM
 */

namespace App\Enum;


class TestEnum
{
    use Enum;

    const BLOOD_SUGAR = 'Blood Sugar';
    const BLOOD_PRESSURE = 'Blood Pressure';
    const PREGNANCY = 'Pregnancy';
    const BODY_TEMPERATURE = 'Body Temperature';
}