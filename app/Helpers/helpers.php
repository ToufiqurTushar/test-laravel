<?php
/**
 * @author Rahat Bashir <rahat.bashir@gmail.com>
 * Date: 10/6/2017
 * Time: 10:41 PM
 */

if(!function_exists('str_to_array')) {
    /**
     * Get an array out of a string with each value white-space-trimmed
     *
     * @param $data
     * @param string $delimiter
     * @return array|null
     */
    function str_to_array($data, $delimiter = ',')
    {
        if ($data && $delimiter) {
            return array_map('trim', explode($delimiter, $data));
        }
        return null;
    }
}

if(!function_exists('arr_to_string')) {
    /**
     * Get a string out of an array
     *
     * @param array $data
     * @param string $glue
     * @return null|string
     */
    function arr_to_string(array $data, $glue = ',')
    {
        if ($data && $glue) {
            return implode($glue, $data);
        }
        return null;
    }
}

if(!function_exists('date_to_day')) {
    /**
     * @param null $date
     * @return null|string
     */
    function date_to_day($date = null, $withTime = false)
    {
        if ($date) {
            $dt = strtotime(substr($date, 0, 10));
            $date = strtotime($date);

            $compare_days = array(
                'yesterday' => 'Yesterday!',
                'yesterday - 1 day' => 'The day before!',
                'today' => 'Today!',
                'tomorrow' => 'Tomorrow!',
                'tomorrow + 1 day' => 'The day after!'
            );

            foreach ($compare_days as $day => $d){
                if(strtotime($day) == $dt){
                    return $withTime ? $d.' @ '.date('g:i A', $date) : $d;
                }
            }
            return $withTime ? date('F d, Y g:i A', $date) : date('F d, Y', $date);
        }
        return null;
    }
}


function role_names($user_role)
{
    $str = '';
    if (!($user_role instanceof \Illuminate\Database\Eloquent\Collection)) {
        return $str;
    }
    foreach ($user_role as $u) {
        if (!$u->role) {
            continue;
        }
        $str .= $u->role->name . ', ';
    }

    return substr($str, 0, -2);
}

function age($dob){
    return \Carbon\Carbon::parse($dob)->age;
}

function qrcode_info($row){
    return json_encode([
       'patient_id'=>$row->patient_id,
        'email'=>$row->email,
        'mobile'=>$row->mobile
    ]);
}