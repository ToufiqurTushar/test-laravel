<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/search/drug', 'Drug\DrugController@search');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth', 'Api\AuthController@store');

Route::group(['middleware'=>['token.checker']],function(){
    Route::resource('/patient', 'Api\PatientController',['only'=>['index','show']]);
    Route::resource('/test-result', 'Api\TestResultController', ['only'=>'store']);
    Route::resource('/agent-report', 'Api\TestResultController', ['only'=>'index']);
});
Route::get('patient-blood-pressure/{patient_id}', 'Api\PatientBloodPressureController@index');
Route::post('patient-blood-pressure', 'Api\PatientBloodPressureController@store');

Route::get('test-types', 'Api\TestController@typeList');

if(false){
    Event::listen(\Illuminate\Database\Events\QueryExecuted::class, function($query)
    {
        echo '<code>'.$query->sql.'</code><br>';
    });
}

