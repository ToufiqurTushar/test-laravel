<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;

Route::get('/test', function(){
   dd(Auth::user()->userFacility->facility->name);
});

Auth::routes();

Route::get('/patient/test-result/{patient_id}', 'Patient\PatientController@testResult');

Route::middleware(['resource.maker','auth.acl'])->group(function () {
    Route::get('/','HomeController@index');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/user','User\UserController@store');
    Route::get('/user','User\UserController@index');
    Route::get('/user/activate/{id}/{state}','User\UserController@activate');
    Route::get('/user/register','User\UserController@create');
    Route::get('/user/remove/{id}','User\UserController@destroy');
    Route::get('/user/edit/{id}','User\UserController@edit');
    Route::post('/user/{id}','User\UserController@update');

    Route::get('/patient/','Patient\PatientController@index');
    Route::get('/patient/show/{id}','Patient\PatientController@show');
    Route::post('/patient','Patient\PatientController@store');
    Route::get('/patient/activate/{id}/{state}','Patient\PatientController@activate');
    Route::get('/patient/register','Patient\PatientController@create');
    Route::get('/patient/remove/{id}','Patient\PatientController@destroy');
    Route::get('/patient/edit/{id}','Patient\PatientController@edit');
    Route::post('/patient/{id}','Patient\PatientController@update');
    Route::get('/patient/history/{id}','Patient\ClinicalManagementController@history');

    Route::get('/disease','Disease\DiseaseController@index');
    Route::get('/disease/show/{id}','Disease\DiseaseController@show');
    Route::post('/disease','Disease\DiseaseController@store');
    Route::get('/disease/activate/{id}/{state}','Disease\DiseaseController@activate');
    Route::get('/disease/add','Disease\DiseaseController@create');
    Route::get('/disease/remove/{id}','Disease\DiseaseController@destroy');
    Route::get('/disease/edit/{id}','Disease\DiseaseController@edit');
    Route::post('/disease/{id}','Disease\DiseaseController@update');

    Route::get('/facility','Facility\FacilityController@index');
    Route::get('/facility/show/{id}','Facility\FacilityController@show');
    Route::post('/facility','Facility\FacilityController@store');
    Route::get('/facility/activate/{id}/{state}','Facility\FacilityController@activate');
    Route::get('/facility/add','Facility\FacilityController@create');
    Route::get('/facility/remove/{id}','Facility\FacilityController@destroy');
    Route::get('/facility/edit/{id}','Facility\FacilityController@edit');
    Route::post('/facility/{id}','Facility\FacilityController@update');
    Route::get('/facility/{id}/user','Facility\FacilityUserController@index');
    Route::get('/facility/{id}/user/add','Facility\FacilityUserController@create');
    Route::get('/facility/user/revoke/{id}','Facility\FacilityUserController@destroy');
    Route::post('/facility/user/add','Facility\FacilityUserController@store');

    Route::get('/drug','Drug\DrugController@index');
    Route::get('/drug/show/{id}','Drug\DrugController@show');
    Route::post('/drug','Drug\DrugController@store');
    Route::get('/drug/activate/{id}/{state}','Drug\DrugController@activate');
    Route::get('/drug/add','Drug\DrugController@create');
    Route::get('/drug/remove/{id}','Drug\DrugController@destroy');
    Route::get('/drug/edit/{id}','Drug\DrugController@edit');
    Route::post('/drug/{id}','Drug\DrugController@update');

    Route::get('/investigation','Investigation\InvestigationController@index');
    Route::get('/investigation/show/{id}','Investigation\InvestigationController@show');
    Route::post('/investigation','Investigation\InvestigationController@store');
    Route::get('/investigation/activate/{id}/{state}','Investigation\InvestigationController@activate');
    Route::get('/investigation/add','Investigation\InvestigationController@create');
    Route::get('/investigation/remove/{id}','Investigation\InvestigationController@destroy');
    Route::get('/investigation/edit/{id}','Investigation\InvestigationController@edit');
    Route::post('/investigation/{id}','Investigation\InvestigationController@update');

    Route::get('/appointment','Appointment\AppointmentController@index');
    Route::get('/appointment/checked-in','Appointment\AppointmentController@checkedIn');
    Route::get('/appointment/canceled','Appointment\AppointmentController@canceled');
    Route::get('/appointment/show/times','Appointment\AppointmentController@showAppointmentTimes');
    Route::get('/appointment/show/{id}','Appointment\AppointmentController@show');
    Route::post('/appointment','Appointment\AppointmentController@store');
    Route::get('/appointment/check-in/{id}/{state}','Appointment\AppointmentController@checkIn');
    Route::get('/appointment/cancel/{id}/{state}','Appointment\AppointmentController@activate');
    Route::get('/appointment/add','Appointment\AppointmentController@create');
    Route::get('/appointment/remove/{id}','Appointment\AppointmentController@destroy');
    Route::get('/appointment/edit/{id}','Appointment\AppointmentController@edit');
    Route::post('/appointment/{id}','Appointment\AppointmentController@update');
    Route::get('/appointment/manage/{id}','Patient\ClinicalManagementController@manage');

    Route::get('/clinical-management/','Patient\ClinicalManagementController@index');
    Route::post('/clinical-management/','Patient\ClinicalManagementController@store');
    Route::get('/clinical-management/visit-dates/{patient_id}', 'Patient\ClinicalManagementController@visitDates');
    Route::get('/clinical-management/visit/{patient_id}/{date}', 'Patient\ClinicalManagementController@visit');

    Route::get('/pharmacy','Pharmacy\PharmacyController@index');
    Route::post('/pharmacy','Pharmacy\PharmacyController@store');
    Route::get('/pharmacy/activate/{id}/{state}','Pharmacy\PharmacyController@activate');
    Route::get('/pharmacy/add','Pharmacy\PharmacyController@create');
    Route::get('/pharmacy/remove/{id}','Pharmacy\PharmacyController@destroy');
    Route::get('/pharmacy/edit/{id}','Pharmacy\PharmacyController@edit');
    Route::post('/pharmacy/{id}','Pharmacy\PharmacyController@update');
});

if(false){
    Event::listen(\Illuminate\Database\Events\QueryExecuted::class, function($query)
    {
        echo '<code>'.$query->sql.'</code><br>';
    });
}