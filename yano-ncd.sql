-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: yano-ncd
-- ------------------------------------------------------
-- Server version	10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments` (
  `appointment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL COMMENT 'for which patient is this appointment?',
  `facilitator_id` int(11) NOT NULL COMMENT 'whom the appointment with?',
  `appointment_date` date NOT NULL COMMENT 'date of appointment',
  `appointment_time` time NOT NULL COMMENT 'time of appointment',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'reason of the appointment in brief',
  `is_checked_in` tinyint(1) NOT NULL DEFAULT '0',
  `checked_in_at` datetime DEFAULT NULL,
  `is_canceled` tinyint(1) NOT NULL DEFAULT '0',
  `canceled_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`appointment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (1,1,1,'2017-10-25','16:30:00','cold, temperature, and pain in whole body',0,NULL,0,NULL,NULL,NULL),(2,2,1,'2017-10-25','17:30:00','cold, temperature, and pain in whole body',0,NULL,0,NULL,NULL,NULL),(3,3,1,'2018-01-17','05:30:00','sick',1,'2018-01-17 23:27:45',0,NULL,'2018-01-15 17:27:25','2018-01-15 17:27:45'),(4,3,1,'2018-02-02','06:00:00','feeling sick',1,'2018-02-02 01:37:44',0,NULL,'2018-02-01 19:37:07','2018-02-01 19:37:44'),(5,3,1,'2018-04-21','01:05:00','big diseses',1,'2018-04-20 01:39:43',0,NULL,'2018-04-19 19:39:39','2018-04-19 19:39:43'),(6,2,1,'2018-05-20','01:05:00','just sick',1,'2018-05-20 01:12:46',0,NULL,'2018-05-19 19:12:34','2018-05-19 19:12:46'),(7,2,2,'2018-05-23','12:05:00','Brief history of illness',1,'2018-05-22 21:56:00',0,NULL,'2018-05-22 15:55:54','2018-05-22 15:56:00');
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_tokens`
--

DROP TABLE IF EXISTS `auth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_tokens` (
  `user_id` int(10) unsigned NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `auth_tokens_token_unique` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_tokens`
--

LOCK TABLES `auth_tokens` WRITE;
/*!40000 ALTER TABLE `auth_tokens` DISABLE KEYS */;
INSERT INTO `auth_tokens` VALUES (4,'$2y$10$.c.XlcRspPsaCeGG3Y2.jOLDdpi1N7DoqjBfcgpmY5KgQNjBjObOa','2018-04-14 20:45:20','2018-04-14 20:45:20'),(4,'$2y$10$0kH55cUQGIgcTb/Z65ct0eUWOdKeto1RQgVjrNrP7Yro2mNaDU/Ey','2018-04-20 17:31:13','2018-04-20 17:31:13'),(4,'$2y$10$9qOjmLqglXClHRSTXnmwHef.DSr2B4OhcZk8.ixu8P2vosxP5A.ES','2018-04-30 20:46:30','2018-04-30 20:46:30'),(4,'$2y$10$A6iXXwl3HGDekjQRUb0nV.A7Hx3wuI9pdvUoxjvsr5v/5R4Jhct7G','2018-04-04 18:14:06','2018-04-04 18:14:06'),(4,'$2y$10$aeh8vVz2qrqkMXz9NZgGEe9polOHKMFxrxP4wVLA61.ZwEqaUZ81i','2018-04-04 18:13:56','2018-04-04 18:13:56'),(4,'$2y$10$bU6RklMt.FFpwSUnbrDzte7rONXLbxdjiOLuf2mNf4cF3wAAnN4na','2018-04-28 16:45:04','2018-04-28 16:45:04'),(2,'$2y$10$bxIJrxpaBciOQG7tX9ZkROLn7BZnaJGtLpWUWfTZnP7rdQHcQwMVK','2018-03-29 19:55:33','2018-03-29 19:55:33'),(4,'$2y$10$bZ7a4zN7pbrwBHmCC0M2leIYONeJFyXObZu2KemmMKPb3j0e.Xf0e','2018-04-04 17:15:33','2018-04-04 17:15:33'),(4,'$2y$10$cLGP6IpV/syDK83Uujq2v.HXnNfUaLm9nk40E4OolXZTZQiTRKY2G','2018-03-29 20:41:12','2018-03-29 20:41:12'),(4,'$2y$10$dq0wTS03mB8Wmc7FialOnuQnu4VerIZG2XRxFGp53NwFWjc2w1J8a','2018-05-01 10:52:33','2018-05-01 10:52:33'),(4,'$2y$10$Gg3GIzYm2VCVkQcc3g9V5uoQvtD1ID612DwCyCvGdeIaiYtdp1oVO','2018-04-05 19:41:33','2018-04-05 19:41:33'),(4,'$2y$10$GNH5qd0Q/cBwLJwhGCx5tOP/1cIZ1TQlx48TrBGbbtK99oxMMvmOS','2018-03-29 19:56:16','2018-03-29 19:56:16'),(4,'$2y$10$GrZz5e1Q7SgaN61O8XZnl.BM7.a0SYQ07CtT7nZLrYk583zn6HiLK','2018-04-11 18:50:30','2018-04-11 18:50:30'),(4,'$2y$10$H0cpPtom55wcpO05yHwmYuwZE0ZYqlMQcaKkyFTYjHXvMjpPMfixi','2018-04-14 20:47:41','2018-04-14 20:47:41'),(4,'$2y$10$IbX3lOM0NL1ibW7FemV21eI/qY3g0JAb71QPOQA06nc7r/Jgf10iG','2018-03-30 17:25:57','2018-03-30 17:25:57'),(4,'$2y$10$iGvMlZMjDo87m/lI4h9.rug1JAyWCogbO2uUo78/BkJ/qVTl5v7Qi','2018-04-29 18:26:00','2018-04-29 18:26:00'),(4,'$2y$10$ireHwduZvq/P1AlPtj/XTOu2Chh0khbuBqnAJ2GBjliuc/vSTUk6K','2018-03-29 20:53:02','2018-03-29 20:53:02'),(4,'$2y$10$K5xbfUHr9/MQkBeGlTIlV.FC2b.tqFnWyNPW2JQVD0D4dAIFwQacK','2018-03-29 21:26:47','2018-03-29 21:26:47'),(4,'$2y$10$KaGyVOqtqRwfTjptY.zfse.5mNstN0iPf2LMrXnICJvwI0HKn26mC','2018-03-31 11:22:26','2018-03-31 11:22:26'),(4,'$2y$10$LA0FTZZ9ajeHLJcihfdFe.hA3XhJjg2TmJxbYbozWfmo5.eam2stq','2018-04-14 20:52:15','2018-04-14 20:52:15'),(4,'$2y$10$LYflvG8tpFZf1U06cDGAR./DzHYx44aZ9dkNtGfTly/MlRZKUvFwG','2018-04-19 06:53:05','2018-04-19 06:53:05'),(2,'$2y$10$MB/2uJtpmZy1RPKxpOR8YuDWbeqYi7UJG74TZIPxiCjsxnPMC9OZO','2018-05-23 13:41:46','2018-05-23 13:41:46'),(4,'$2y$10$NaKz/54LYHYt4fnhUY.0xOsPCWUsU/XGgyZhCTYispeo6uLoSpQi.','2018-06-02 20:45:00','2018-06-02 20:45:00'),(4,'$2y$10$nh5VwOsBhAZnDfKokAxF5eR1ojuOaMF7gwZOQve4JFs4G9k.h70C2','2018-04-15 12:10:40','2018-04-15 12:10:40'),(4,'$2y$10$Nn6xP8dyta6f0IJ5i2.p2.5h945stThloMKEBJpnAhEYIcrolVhbW','2018-04-30 20:38:24','2018-04-30 20:38:24'),(4,'$2y$10$nvfOyS51GXgxzF4KeXX0VeTL6lhc1AQNwZtiTUKax9AeyUnp3897i','2018-06-04 12:01:17','2018-06-04 12:01:17'),(4,'$2y$10$oeB3.es4ZDYP8Br5DgphT.PIc5pXvtgwBcC8oKj/OW3XYLt3rAZR.','2018-04-28 16:46:03','2018-04-28 16:46:03'),(4,'$2y$10$oNCHXcMKfYvC0/aRUJNKy.Jv6JJaqx13UTjmZpfnwHZz5Ad78IQgm','2018-05-03 20:54:00','2018-05-03 20:54:00'),(4,'$2y$10$ShnLpJLu4h9ZdX9G50pLU.ys1d00A1tJSGoy/MWXvU/3cTo/6iIpC','2018-04-14 20:57:29','2018-04-14 20:57:29'),(4,'$2y$10$tfem5fAhwU/r0S26nWDoWOJPc.7Fyn5EY5k8GlI58aYxSEKvEeXfu','2018-04-14 20:45:29','2018-04-14 20:45:29'),(4,'$2y$10$tJNeV2XyKLE2KwFU8sgJMOOPqikFShTuwmlXqoI5w7KWoBVCk.uUe','2018-04-20 17:31:17','2018-04-20 17:31:17'),(4,'$2y$10$tK6cZWnQLAFIC46kYkbWzeOVTZagDfNHxYQtLSnfHbpVGQ9ftT1Gi','2018-03-31 16:49:11','2018-03-31 16:49:11'),(4,'$2y$10$tZYUK/6Y6AizQjydvrw5MOxNLqFdP34Q80pIvTpnNEz/7274UN9D.','2018-04-20 17:35:39','2018-04-20 17:35:39'),(4,'$2y$10$UONXXKYwn7y94yyxOP1UAe22ylhKuVwTTZkrnkLIhLp4dNWmU/one','2018-03-29 20:51:51','2018-03-29 20:51:51'),(4,'$2y$10$vtl1PSKRiyjEvxRTmegnF.6UTyxPrIhif0PNdZTxDpW2gE/HIh4ze','2018-05-13 11:17:21','2018-05-13 11:17:21'),(4,'$2y$10$w3akvKFdkwrkyE7h8NLPu.DJxXJz2zJel0niiLqSqDgF.EYX0oYf2','2018-05-03 20:16:31','2018-05-03 20:16:31'),(4,'$2y$10$W5Gbxzr6WtJE96x5lMfOq.nZ9jgJsF4ZTB.e.Te7ADnCYlOVvBzQ6','2018-04-11 18:50:13','2018-04-11 18:50:13'),(4,'$2y$10$wF1zgc2FQVXGGrUO4OYuMuLubOflTjVFcqyAKGMwEe88oBD9POk6C','2018-04-30 11:39:43','2018-04-30 11:39:43'),(4,'$2y$10$wO0Fra.1eShffiJAxjAMK.hVITIL2.SdF4HXxx/GJxfnCvhGnca1S','2018-04-15 12:10:43','2018-04-15 12:10:43'),(4,'$2y$10$wrlLAnkfBANI0sKCtb/u.uv2MgwbAtZ80SK3TrM3vPKjdmCeQemD.','2018-04-14 20:45:47','2018-04-14 20:45:47'),(4,'$2y$10$ZDRq0wSGOTOrgsjS/vcnI.3wa.KJJ7uah/QgxaBOvV8jR4G1CZAJ6','2018-06-10 20:21:19','2018-06-10 20:21:19');
/*!40000 ALTER TABLE `auth_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinical_managements`
--

DROP TABLE IF EXISTS `clinical_managements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinical_managements` (
  `clinical_management_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visit_date` date NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `doctor_id` int(10) unsigned NOT NULL,
  `past_illness` text COLLATE utf8mb4_unicode_ci,
  `routine` text COLLATE utf8mb4_unicode_ci,
  `present_complaints` text COLLATE utf8mb4_unicode_ci,
  `medical_history` text COLLATE utf8mb4_unicode_ci,
  `medication` text COLLATE utf8mb4_unicode_ci,
  `personal` text COLLATE utf8mb4_unicode_ci,
  `family` text COLLATE utf8mb4_unicode_ci,
  `social` text COLLATE utf8mb4_unicode_ci,
  `allergy` text COLLATE utf8mb4_unicode_ci,
  `psychiatric_illness` text COLLATE utf8mb4_unicode_ci,
  `appearance` text COLLATE utf8mb4_unicode_ci,
  `anaemia` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_pressure` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `venous_pressure` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jaundice` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `temperature` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clubbing` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cyanosis` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pulse` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oedema` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_examination` text COLLATE utf8mb4_unicode_ci,
  `cardiac_vascular_system` text COLLATE utf8mb4_unicode_ci,
  `respiratory_system` text COLLATE utf8mb4_unicode_ci,
  `elementary_system` text COLLATE utf8mb4_unicode_ci,
  `musculoskeletal_system` text COLLATE utf8mb4_unicode_ci,
  `investigation_specific` text COLLATE utf8mb4_unicode_ci,
  `diagnosis` text COLLATE utf8mb4_unicode_ci,
  `diagnosis_remarks` text COLLATE utf8mb4_unicode_ci,
  `meds` text COLLATE utf8mb4_unicode_ci,
  `tests` text COLLATE utf8mb4_unicode_ci,
  `medication_advice` text COLLATE utf8mb4_unicode_ci,
  `menstruation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menstruation_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `para` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gravida` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menarche` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menopause` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`clinical_management_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinical_managements`
--

LOCK TABLES `clinical_managements` WRITE;
/*!40000 ALTER TABLE `clinical_managements` DISABLE KEYS */;
INSERT INTO `clinical_managements` VALUES (1,'2018-02-02',1,2,'[\"Hypertension\",\"Bronchial Asthma\"]','[\"Liver Function Test\",\"Serum Creatinine\",\"Serum Electrolyte\"]','adsfasdf',NULL,'asdfa','[]','adfa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[{\"drug_name\":\"Napa\",\"dose\":\"dose\",\"freq\":\"freq\",\"duration\":\"dur\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-01 18:43:54','2018-02-01 18:58:38'),(2,'2018-03-16',2,2,'[\"Diabetics\",\"Hypertension\",\"Bronchial Asthma\",\"COPD\"]','[\"FBC\\/CBC\",\"Serum Electrolyte\",\"Serum Creatinine\",\"Liver Function Test\"]',NULL,NULL,NULL,'[]',NULL,NULL,NULL,NULL,'appearance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[{\"drug_name\":\"Ace\",\"dose\":\"200mg\",\"freq\":\"20 days\",\"duration\":\"1 month\"}]','[{\"test_type\":\"BLOOD_SUGAR\",\"is_repeat\":true,\"repeat_frequency\":\"week\",\"repeat_until\":\"2018-05-01\"}]','other advice',NULL,NULL,NULL,NULL,NULL,NULL,'2018-03-15 19:56:12','2018-03-15 19:56:12'),(3,'2018-04-20',2,2,'[\"Hypertension\",\"Bronchial Asthma\",\"COPD\"]','[\"Chest-X-ray\"]','Present complaints',NULL,NULL,'[]',NULL,NULL,NULL,NULL,NULL,'Anaemia','Blood Pressure','Venous Pressure',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[{\"is_repeat\":true,\"repeat_frequency\":\"week\",\"test_type\":\"BLOOD_SUGAR\",\"repeat_until\":\"2018-06-26\"},{\"test_type\":\"BLOOD_PRESSURE\",\"is_repeat\":true,\"repeat_frequency\":\"week\",\"repeat_until\":\"2018-07-31\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-19 19:52:48','2018-04-19 19:52:48');
/*!40000 ALTER TABLE `clinical_managements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designations`
--

DROP TABLE IF EXISTS `designations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designations`
--

LOCK TABLES `designations` WRITE;
/*!40000 ALTER TABLE `designations` DISABLE KEYS */;
INSERT INTO `designations` VALUES (1,'CEO',0,NULL,NULL);
/*!40000 ALTER TABLE `designations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diseases`
--

DROP TABLE IF EXISTS `diseases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diseases` (
  `disease_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'name of the disease',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'description about the disease',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'if the disease is allowed to be treated in the facility',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`disease_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diseases`
--

LOCK TABLES `diseases` WRITE;
/*!40000 ALTER TABLE `diseases` DISABLE KEYS */;
INSERT INTO `diseases` VALUES (1,'Tuberculosis','Tuberculosis is also known as TB, a curable disease but many people still think that TB is not curable!!!',1,NULL,NULL),(2,'HIV','Human Immunodeficiency Virus (HIV) known as HIV aids is a non curable disease!!! Only awareness and protective measurements can save from this!!!',1,NULL,NULL);
/*!40000 ALTER TABLE `diseases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drugs`
--

DROP TABLE IF EXISTS `drugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drugs` (
  `drug_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'common brand name of the drug given by pharmaceutical for marketing',
  `generic_name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'the generic name of the drug',
  `dosage_form` enum('Tablet','Capsule','Injection','Syrup') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'the form of dosage like table/capsule/syrup',
  `dosage_unit` enum('mg','ml') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'the unit of a dose like mg/ml etc...',
  `age_group` enum('Adult','Children') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'the age group for which this drug is applicable',
  `administration_route` enum('Oral','Sublingual','Buccal','Rectal') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'the channels of taking this drug',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'short description on the drug',
  `indication` text COLLATE utf8mb4_unicode_ci COMMENT 'disease indication for usage',
  `photo` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'photograph of the drug',
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT 'is the drug is allowed to be prescribed, default is "yes"',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`drug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drugs`
--

LOCK TABLES `drugs` WRITE;
/*!40000 ALTER TABLE `drugs` DISABLE KEYS */;
INSERT INTO `drugs` VALUES (1,'Napa','Paracetamol','Tablet','mg','Adult','Oral','Paracetamol is a fast acting and safe analgesic with marked antipyretic property. It is specially suitable for aptients who for any reason can not tolerate aspirin or other analgesics etc...','All conditions requiring relief from apin and fever such as neuritis, neuralgia, headache, erache, toothache, pain due to rheumatic disorder, cold, influenza, dysmenorrhoea, post-vaccination pain and fever of children etc...',NULL,'0',NULL,'2018-02-03 12:06:22'),(2,'Ace','Paracetamol','Syrup','ml','Children','Oral','Paracetamol is a fast acting and safe analgesic with marked antipyretic property. It is specially suitable for aptients who for any reason can not tolerate aspirin or other analgesics etc...','All conditions requiring relief from apin and fever such as neuritis, neuralgia, headache, erache, toothache, pain due to rheumatic disorder, cold, influenza, dysmenorrhoea, post-vaccination pain and fever of children etc...',NULL,'1',NULL,NULL);
/*!40000 ALTER TABLE `drugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facilities`
--

DROP TABLE IF EXISTS `facilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facilities` (
  `facility_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'name of the contact person, may be non-medical',
  `email` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'physical address of the facility',
  `specialization` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'names of diseases for which the facility is special',
  `engagement_date` date DEFAULT NULL COMMENT 'the date of enrollment under the ncd project',
  `photo` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'photograph of the facility',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'is the facility is active in providing service under the ncd project, default is "yes"',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`facility_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facilities`
--

LOCK TABLES `facilities` WRITE;
/*!40000 ALTER TABLE `facilities` DISABLE KEYS */;
INSERT INTO `facilities` VALUES (1,'Uttara Clinic','Contact of Uttara Clinic','uttara@example.com','01736545637','887464683','Physical address of Uttara clinic','Tuberculosis',NULL,NULL,1,NULL,NULL),(2,'Jatrabari Clinic','Contact of Jatrabari Clinic','jatrabari@example.com','01736545637','887464683','Physical address of Jatrabari clinic','HIV',NULL,NULL,1,NULL,NULL),(3,'Brendan Sweet','Eu voluptatem unde dolores distinctio Eos non','gyxiferyh@yahoo.com','617','+283-40-5830916','Quibusdam obcaecati voluptate beatae ipsam a dicta tempor quaerat ipsum quibusdam tenetur voluptates','Voluptatum sequi voluptatibus quia laboris sint quo accusantium sunt','1993-06-17',NULL,1,'2018-01-15 17:21:10','2018-01-15 17:21:10');
/*!40000 ALTER TABLE `facilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facility_users`
--

DROP TABLE IF EXISTS `facility_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility_users` (
  `facility_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `facility_id` int(10) unsigned NOT NULL COMMENT 'id of clinic facility',
  `user_id` int(10) unsigned NOT NULL COMMENT 'id of clinic facility',
  `assign_date` date DEFAULT NULL COMMENT 'when the user was assigned to this facility',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`facility_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facility_users`
--

LOCK TABLES `facility_users` WRITE;
/*!40000 ALTER TABLE `facility_users` DISABLE KEYS */;
INSERT INTO `facility_users` VALUES (1,1,1,NULL,NULL,NULL),(2,1,2,NULL,NULL,NULL),(3,2,1,NULL,NULL,NULL),(4,2,2,NULL,NULL,NULL);
/*!40000 ALTER TABLE `facility_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investigations`
--

DROP TABLE IF EXISTS `investigations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investigations` (
  `investigation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'title of the medical investigation',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'brief description about the investigation',
  `params` text COLLATE utf8mb4_unicode_ci COMMENT 'json encoded result parameters of this investigation',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'if the investigation is allowed to be prescribed in the facility',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`investigation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investigations`
--

LOCK TABLES `investigations` WRITE;
/*!40000 ALTER TABLE `investigations` DISABLE KEYS */;
INSERT INTO `investigations` VALUES (1,'Hepatitis B Virus Testing','Hepatitis B serologic testing involves measurement of several hepatitis B virus (HBV)-specific antigens and antibodies. Different serologic “markers” or combinations of markers are used to identify different phases of HBV infection and to determine whether a patient has acute or chronic HBV infection is immune to HBV as a result of prior infection or vaccination, or is susceptible to infection','HBsAg,anti-HBc,anti-HBs,IgM anti-HBc,HBeAg,HBV DNA',1,NULL,NULL),(2,'CBC','CBC is a very important medical investigation of Blood. The full meaning of CBC is Complete Blood Count!!','Hemoglobin, RBC, WBC, Hct, MCV,Platelets',1,NULL,NULL);
/*!40000 ALTER TABLE `investigations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (24,'2014_10_12_000000_create_users_table',1),(25,'2014_10_12_100000_create_password_resets_table',1),(26,'2015_12_22_103904_create_roles_table',1),(27,'2015_12_22_103939_create_resources_table',1),(28,'2015_12_22_103949_create_permissions_table',1),(29,'2017_02_07_174058_create_user_roles_table',1),(30,'2017_09_21_110850_create_designations_table',1),(31,'2017_09_24_060501_create_facility_table',1),(32,'2017_09_24_060806_create_drug_table',1),(33,'2017_09_24_060944_create_disease_table',1),(34,'2017_09_24_061052_create_investigation_table',1),(35,'2017_09_24_061131_create_patient_table',1),(36,'2017_10_04_124928_create_appointments_table',1),(37,'2017_11_10_233654_create_facility_users_table',1),(38,'2017_12_19_005733_create_clinical_managements_table',1),(39,'2018_02_03_153505_create_pharmacies_table',2),(40,'2016_11_09_154459_create_auth_tokens_table',3),(42,'2018_04_29_221518_create_test_results_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `patient_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'physical address of the facility',
  `religion` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `civil_status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date DEFAULT NULL COMMENT 'date of birth of the patient',
  `birth_place` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'name of the place, where the patient was born',
  `blood_group` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `national_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'national id of the patient',
  `insurance_company` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'id of the insurance company, if the patient has health insurance',
  `insurance_policy_number` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'health policy id number, if there is any',
  `photo` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'photograph of the patient',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'is the patient is active, default is "yes"',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,'Rahat Bashir','880989284','8801715812079','rahat.bashir@yano.tech','Address of Rahat Bashir','Islam','Married','Male','1976-09-03','Dhaka','B+','938294729387979223423',NULL,NULL,NULL,1,NULL,NULL),(2,'Mahabubul Hasan','880989284','01717508422','mahabubul@yano.tech','Address of Rahat Bashir','Islam','Single','Male','1976-09-03','Dhaka','B+','938294729387979223423',NULL,NULL,NULL,1,NULL,NULL),(3,'Rahim Callahan','+459-82-2448742','881','qydyce@hotmail.com','Quis qui id ullam ab et','Christianity','Widowed','Male','2004-02-14','Consequat Consequuntur quis a odio','A+','2131231213','Kaufman and Park Inc','527',NULL,1,'2018-01-15 17:12:14','2018-01-15 17:12:14');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `permission_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `resource_id` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,'a3086d6dbdcc7267892ee0e0370f7f3d986acfd2'),(2,1,'807f24a61b7d682dbefb04adc54af42c17db0ad8'),(3,1,'9947237701b3e2e08f389fb42961948f8481ff4d'),(4,1,'5254b01b286f0e9786669448524a833d474b58fc'),(5,1,'f5ac52e6c79edb2d2d6c79949c815162a23e9637'),(6,1,'07bf7ccff6919d66a6098a3cd30afe730adf930b'),(7,1,'0f10abe8d2d513ac1f22ea4a8bca4dfd7128bd42'),(8,1,'c3ec459ee280417e4890b80a985d170d9b5f54a6'),(9,1,'406c304551974a8c0e3be28ebee984b0e767e911'),(10,1,'393efc569b2e9b94660d50a85dc4c06dc2835144'),(11,1,'40635ffee1ca2d931215f29fa79203794a7b9bf9'),(12,1,'165e9d8e6a64600cc0d02270113a9d6100cf9591'),(13,1,'4c778363e5e22e833a3cb41a6527c46c7e29bc2d'),(14,1,'1c9b41b77af835b0a65ba090071008094adddb1e'),(15,1,'eb05927bd7a3cc13bbb4cd7fe0855624c4b3c864'),(16,1,'0be54e3a796e29b614945818a39d2feacd8be991'),(17,1,'2a35a797ea693b386a563a62f5bbeeb5b40ca6ed'),(18,1,'9c85cdba521a2dafd7a7c8e9af23016c9718c552'),(19,1,'cdf8b29d49ac4c5dddceffec6d03c1437d56d04f'),(20,1,'bc8c17f1d566d6d8dcd5044d320e64bceb44ce49'),(21,1,'1ca88e6ab82215be70592b84d428ce313aa9d630'),(22,1,'0527d1e577567b821c9cc2e8f22c1edf99b3a393'),(23,1,'6dac0264488bff4929d22a4e195bda297d10949a'),(24,1,'032c4505f8582061b9dbbd461ad94abae895b6c1'),(25,1,'67057b8563c07cf068b87a88bc1d91818e831de7'),(26,1,'84493e934d70b21cc5f64970dc8481ea4412b4f2'),(27,1,'01f92a85636c76a0e6bc03ac1d6a4e1e69c4ec17'),(28,1,'2d00f4418cad2d8ba7ab496a915cb97e71251861'),(29,1,'0800bdfa68120aecee5e57b807e056ab20079574'),(30,1,'8ffb04cc2efe7657d1dd30a92ff76ff0dfa18481'),(31,1,'30eb4296139dd2bb4e9d912eb25a594f224097cd'),(32,1,'34d3909e22ed8413a4d0cb1cd2310e2730cb51d9'),(33,1,'5659f267809b80f94b9f9cd34cd9bdb33aebb4a3'),(34,1,'be52bd0ffba8b93e43a9e13d9acfd7b03c584ae6'),(35,1,'88c4356f35d59c4999593fea5fc9f6ac95a2c100'),(36,1,'02845b64fc5fe4078e8d3059678766fb0f0f9c5d'),(37,1,'892b13a1847acb88d555bf37a5ae838eaedcd0fc'),(38,1,'15084c5c5be1a342b97504e4fdceac52bc43566a'),(39,1,'ed2bf49c97e061e99cc3616b65518d49737b8aba'),(40,1,'74f1676817eda38c1d9eb60f3722ebd9a73e9aca'),(41,1,'d49e54f8517151ee055075b22ca9945214766d52'),(42,1,'d85ef84219d0a23c506cf1d0aeddd3264cf5579c'),(43,1,'8e1cd5f1d77985c721c5c39b9e58c32aba8b54ab'),(44,1,'018643a340fef5b749cf890fb4ba3bc8fc1328e8'),(45,1,'478d43bfe8276358bafe245206c540dc6c6294e0'),(46,1,'bc58a6b79130434631adb0d4a40706f93f894707'),(47,1,'4423a3f3fcd08a60d4e9782b76575040802b72e4'),(48,1,'e6e139e0827c06938d965507944321c14b3e8045'),(49,1,'ec801b996343e1eaff93e60a4290621bb0d9001d'),(50,1,'cd878fad8f9be51d72f333e311a68e7e2e16ca1b'),(51,1,'879be83bce82c56793c9f2f0427f6f6b30a24dc4'),(52,1,'870570dedfc96f0d87ece47aeab5bd1b6773ef7d'),(53,1,'40e289ed8e312aeb924cafd957dd40f6800bd9a2'),(54,1,'4eb19817866ede26559fe5ba32b2dfb8bbcc7c0a'),(55,1,'e2c82ca8a954b7cbc50f45502a585f4b35586c30'),(56,1,'c5e26507f3bc0bfb3b0dfcc47e2478bb9618ec71'),(57,1,'7245ba44bfcad02e7afe5bb9c5fe450e867f6ec0'),(58,1,'015af64ca4949d21a7860e3506ddd4d9e89760dc'),(59,1,'77248118447a75949819c0a5fa0316768014a123'),(60,1,'90625ee1ee38a94ce55f9353df9d9d8ec3ee6ee7'),(61,1,'c4f8fe2e6e2b9ca86281bddca6fdab502bd4d13b'),(62,1,'aba6f27bcb85a6ddfafd7827782d912021aac0be'),(63,1,'3346ab41b8dd8b05c6cc277e3b0c0e50babdd065'),(64,1,'c27aa6936b63e1bd98cc7c756d85bfd26009fc3d'),(65,1,'c6d059ab811935a566f98ed3b084adab9f3e6742'),(66,1,'339d4932c11d8912beae0ba7d79dceea9cb3cac4'),(67,1,'b2fb73fe2e8408f40204e7fd7a0b12321c0819d7'),(68,1,'cc35f16bb45fd7e945ccf1c4b2c5ae87970c6188'),(69,1,'cadb4ae8a22d57dc1926084f4715fe7a2c61c8c6'),(70,1,'45f4df7aa113c4c07d070e65675ac085a1baab03'),(71,1,'91029119e2122c64ee6dc45455c304ecd39508b3'),(72,1,'ae85a69bbf128046f36c541ec0f159cab32a4c52'),(73,1,'1b0cb7708b54cf5c672dab4638ae5320a3b1f000'),(74,1,'9b5890d6150e863e54adc3ba2fcb8f489e4ddaa6'),(75,1,'d0944f12517751077a7efca43c8d97aea52c49e4'),(76,1,'907722046dc4caa4fa6b7d0ec3882963a0e40ab5'),(77,1,'99d829b030fa992d75189fdc34c5db1c11f45d4a'),(78,1,'4c82d4bc5f4419321e75b5ec57284d68dfcc01af'),(79,1,'e769f43b0a2c57fb6709356daa5fce5422baa2ac'),(80,1,'01d98d6b942ab54a4ee0337d1750f4364f51f007'),(81,1,'1c848437b9c3724ee7d0a8ea2a84ea43bfdbaae2'),(82,1,'8b0c104694f630b72a7809ecd49806bb50f7d92e'),(83,1,'291f38e201cb768875fe9b88215b82eb9cf702fc'),(84,1,'1efca26eea169a2b52525ea1e6cb7a728fd36f53'),(85,1,'937d5b7edcdbe8eecd0fecbd9db220a4486831b3'),(86,1,'4035eabfcef1426d412640fd65d1b7947f7d34ae'),(87,1,'d5d2f9afb84562679a303fd6bd10d5ac0abc6b0a'),(88,1,'4a0522a3f6868fff37a60ebf5a87a83f6d1d8ad0'),(89,1,'a3aa481521fbce129d836e3132841c3386ad3411'),(90,1,'61c3786a18eeb9fd0c5337d27011db3a150adb2b'),(91,1,'93b4cc9fd3df15b3074ed39696c804c33d782009');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pharmacies`
--

DROP TABLE IF EXISTS `pharmacies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pharmacies` (
  `pharmacy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pharmacy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pharmacies`
--

LOCK TABLES `pharmacies` WRITE;
/*!40000 ALTER TABLE `pharmacies` DISABLE KEYS */;
INSERT INTO `pharmacies` VALUES (1,4,'Anurag Pharmacy Ltd.','Mohammadpur, Dhaka','01717508422',1,'2018-02-03 12:02:59','2018-02-03 15:58:53');
/*!40000 ALTER TABLE `pharmacies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `resource_id` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `controller` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`resource_id`),
  UNIQUE KEY `resources_action_unique` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES ('015af64ca4949d21a7860e3506ddd4d9e89760dc','Investigation-Investigation GET::Index','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@index','2018-01-15 17:09:09','2018-01-15 17:09:09'),('018643a340fef5b749cf890fb4ba3bc8fc1328e8','Facility-Facility GET::Edit','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@edit','2018-01-15 17:09:08','2018-01-15 17:09:08'),('01d98d6b942ab54a4ee0337d1750f4364f51f007','Patient-ClinicalManagement GET::VisitDates','Patient-ClinicalManagement','App\\Http\\Controllers\\Patient\\ClinicalManagementController@visitDates','2018-01-15 17:09:11','2018-01-15 17:09:11'),('01f92a85636c76a0e6bc03ac1d6a4e1e69c4ec17','Patient-Patient GET::Destroy','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@destroy','2018-01-15 17:09:07','2018-01-15 17:09:07'),('02845b64fc5fe4078e8d3059678766fb0f0f9c5d','Disease-Disease GET::Edit','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@edit','2018-01-15 17:09:08','2018-01-15 17:09:08'),('032c4505f8582061b9dbbd461ad94abae895b6c1','Patient-Patient GET::Show','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@show','2018-01-15 17:09:07','2018-01-15 17:09:07'),('0527d1e577567b821c9cc2e8f22c1edf99b3a393','User-User POST::Update','User-User','App\\Http\\Controllers\\User\\UserController@update','2018-01-15 17:09:06','2018-01-15 17:09:06'),('07bf7ccff6919d66a6098a3cd30afe730adf930b','Role POST::Update','User-Role','Uzzal\\Acl\\Http\\RoleController@update','2018-01-15 17:01:13','2018-01-15 17:01:13'),('0800bdfa68120aecee5e57b807e056ab20079574','Patient-Patient POST::Update','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@update','2018-01-15 17:09:07','2018-01-15 17:09:07'),('0be54e3a796e29b614945818a39d2feacd8be991','User-User POST::Store','User-User','App\\Http\\Controllers\\User\\UserController@store','2018-01-15 17:09:06','2018-01-15 17:09:06'),('0f10abe8d2d513ac1f22ea4a8bca4dfd7128bd42','Resource GET::Index','User-Resource','Uzzal\\Acl\\Http\\ResourceController@index','2018-01-15 17:01:13','2018-01-15 17:01:13'),('15084c5c5be1a342b97504e4fdceac52bc43566a','Facility-Facility GET::Index','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@index','2018-01-15 17:09:08','2018-01-15 17:09:08'),('165e9d8e6a64600cc0d02270113a9d6100cf9591','Resource POST::Update','User-Resource','Uzzal\\Acl\\Http\\ResourceController@update','2018-01-15 17:01:13','2018-01-15 17:01:13'),('1b0cb7708b54cf5c672dab4638ae5320a3b1f000','Appointment-Appointment GET::Activate','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@activate','2018-01-15 17:09:11','2018-01-15 17:09:11'),('1c848437b9c3724ee7d0a8ea2a84ea43bfdbaae2','Patient-ClinicalManagement GET::Visit','Patient-ClinicalManagement','App\\Http\\Controllers\\Patient\\ClinicalManagementController@visit','2018-01-15 17:09:11','2018-01-15 17:09:11'),('1c9b41b77af835b0a65ba090071008094adddb1e','Home GET::Index','Home','App\\Http\\Controllers\\HomeController@index','2018-01-15 17:01:58','2018-01-15 17:01:58'),('1ca88e6ab82215be70592b84d428ce313aa9d630','User-User GET::Edit','User-User','App\\Http\\Controllers\\User\\UserController@edit','2018-01-15 17:09:06','2018-01-15 17:09:06'),('1efca26eea169a2b52525ea1e6cb7a728fd36f53','Pharmacy-Pharmacy POST::Store','Pharmacy-Pharmacy','App\\Http\\Controllers\\Pharmacy\\PharmacyController@store','2018-02-03 11:32:28','2018-02-03 11:32:28'),('291f38e201cb768875fe9b88215b82eb9cf702fc','Pharmacy-Pharmacy GET::Index','Pharmacy-Pharmacy','App\\Http\\Controllers\\Pharmacy\\PharmacyController@index','2018-02-03 11:29:16','2018-02-03 11:29:16'),('2a35a797ea693b386a563a62f5bbeeb5b40ca6ed','User-User GET::Index','User-User','App\\Http\\Controllers\\User\\UserController@index','2018-01-15 17:09:06','2018-01-15 17:09:06'),('2d00f4418cad2d8ba7ab496a915cb97e71251861','Patient-Patient GET::Edit','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@edit','2018-01-15 17:09:07','2018-01-15 17:09:07'),('30eb4296139dd2bb4e9d912eb25a594f224097cd','Disease-Disease GET::Show','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@show','2018-01-15 17:09:07','2018-01-15 17:09:07'),('3346ab41b8dd8b05c6cc277e3b0c0e50babdd065','Investigation-Investigation GET::Destroy','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@destroy','2018-01-15 17:09:10','2018-01-15 17:09:10'),('339d4932c11d8912beae0ba7d79dceea9cb3cac4','Appointment-Appointment GET::Index','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@index','2018-01-15 17:09:10','2018-01-15 17:09:10'),('34d3909e22ed8413a4d0cb1cd2310e2730cb51d9','Disease-Disease POST::Store','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@store','2018-01-15 17:09:07','2018-01-15 17:09:07'),('393efc569b2e9b94660d50a85dc4c06dc2835144','Resource GET::Destroy','User-Resource','Uzzal\\Acl\\Http\\ResourceController@destroy','2018-01-15 17:01:13','2018-01-15 17:01:13'),('4035eabfcef1426d412640fd65d1b7947f7d34ae','Pharmacy-Pharmacy GET::Create','Pharmacy-Pharmacy','App\\Http\\Controllers\\Pharmacy\\PharmacyController@create','2018-02-03 11:32:28','2018-02-03 11:32:28'),('40635ffee1ca2d931215f29fa79203794a7b9bf9','Resource POST::Store','User-Resource','Uzzal\\Acl\\Http\\ResourceController@store','2018-01-15 17:01:13','2018-01-15 17:01:13'),('406c304551974a8c0e3be28ebee984b0e767e911','Resource GET::Edit','User-Resource','Uzzal\\Acl\\Http\\ResourceController@edit','2018-01-15 17:01:13','2018-01-15 17:01:13'),('40e289ed8e312aeb924cafd957dd40f6800bd9a2','Drug-Drug GET::Activate','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@activate','2018-01-15 17:09:09','2018-01-15 17:09:09'),('4423a3f3fcd08a60d4e9782b76575040802b72e4','Facility-FacilityUser GET::Create','Facility-FacilityUser','App\\Http\\Controllers\\Facility\\FacilityUserController@create','2018-01-15 17:09:09','2018-01-15 17:09:09'),('45f4df7aa113c4c07d070e65675ac085a1baab03','Appointment-Appointment GET::Show','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@show','2018-01-15 17:09:10','2018-01-15 17:09:10'),('478d43bfe8276358bafe245206c540dc6c6294e0','Facility-Facility POST::Update','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@update','2018-01-15 17:09:08','2018-01-15 17:09:08'),('4a0522a3f6868fff37a60ebf5a87a83f6d1d8ad0','Pharmacy-Pharmacy GET::Edit','Pharmacy-Pharmacy','App\\Http\\Controllers\\Pharmacy\\PharmacyController@edit','2018-02-03 11:32:28','2018-02-03 11:32:28'),('4c778363e5e22e833a3cb41a6527c46c7e29bc2d','Patient-Patient GET::Create','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@create','2018-01-15 17:01:30','2018-01-15 17:01:30'),('4c82d4bc5f4419321e75b5ec57284d68dfcc01af','Patient-ClinicalManagement GET::Index','Patient-ClinicalManagement','App\\Http\\Controllers\\Patient\\ClinicalManagementController@index','2018-01-15 17:09:11','2018-01-15 17:09:11'),('4eb19817866ede26559fe5ba32b2dfb8bbcc7c0a','Drug-Drug GET::Create','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@create','2018-01-15 17:09:09','2018-01-15 17:09:09'),('5254b01b286f0e9786669448524a833d474b58fc','Role GET::Destroy','User-Role','Uzzal\\Acl\\Http\\RoleController@destroy','2018-01-15 17:01:13','2018-01-15 17:01:13'),('5659f267809b80f94b9f9cd34cd9bdb33aebb4a3','Disease-Disease GET::Activate','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@activate','2018-01-15 17:09:07','2018-01-15 17:09:07'),('61c3786a18eeb9fd0c5337d27011db3a150adb2b','Patient-ClinicalManagement GET::Manage','Patient-ClinicalManagement','App\\Http\\Controllers\\Patient\\ClinicalManagementController@manage','2018-04-19 19:40:00','2018-04-19 19:40:00'),('67057b8563c07cf068b87a88bc1d91818e831de7','Patient-Patient POST::Store','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@store','2018-01-15 17:09:07','2018-01-15 17:09:07'),('6dac0264488bff4929d22a4e195bda297d10949a','Patient-Patient GET::Index','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@index','2018-01-15 17:09:06','2018-01-15 17:09:06'),('7245ba44bfcad02e7afe5bb9c5fe450e867f6ec0','Drug-Drug POST::Update','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@update','2018-01-15 17:09:09','2018-01-15 17:09:09'),('74f1676817eda38c1d9eb60f3722ebd9a73e9aca','Facility-Facility POST::Store','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@store','2018-01-15 17:09:08','2018-01-15 17:09:08'),('77248118447a75949819c0a5fa0316768014a123','Investigation-Investigation GET::Show','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@show','2018-01-15 17:09:09','2018-01-15 17:09:09'),('807f24a61b7d682dbefb04adc54af42c17db0ad8','Role GET::Create','User-Role','Uzzal\\Acl\\Http\\RoleController@create','2018-01-15 17:01:13','2018-01-15 17:01:13'),('84493e934d70b21cc5f64970dc8481ea4412b4f2','Patient-Patient GET::Activate','Patient-Patient','App\\Http\\Controllers\\Patient\\PatientController@activate','2018-01-15 17:09:07','2018-01-15 17:09:07'),('870570dedfc96f0d87ece47aeab5bd1b6773ef7d','Drug-Drug POST::Store','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@store','2018-01-15 17:09:09','2018-01-15 17:09:09'),('879be83bce82c56793c9f2f0427f6f6b30a24dc4','Drug-Drug GET::Show','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@show','2018-01-15 17:09:09','2018-01-15 17:09:09'),('88c4356f35d59c4999593fea5fc9f6ac95a2c100','Disease-Disease GET::Destroy','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@destroy','2018-01-15 17:09:07','2018-01-15 17:09:07'),('892b13a1847acb88d555bf37a5ae838eaedcd0fc','Disease-Disease POST::Update','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@update','2018-01-15 17:09:08','2018-01-15 17:09:08'),('8b0c104694f630b72a7809ecd49806bb50f7d92e','Appointment-Appointment GET::Manage','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@manage','2018-01-15 17:30:18','2018-01-15 17:30:18'),('8e1cd5f1d77985c721c5c39b9e58c32aba8b54ab','Facility-Facility GET::Destroy','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@destroy','2018-01-15 17:09:08','2018-01-15 17:09:08'),('8ffb04cc2efe7657d1dd30a92ff76ff0dfa18481','Disease-Disease GET::Index','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@index','2018-01-15 17:09:07','2018-01-15 17:09:07'),('90625ee1ee38a94ce55f9353df9d9d8ec3ee6ee7','Investigation-Investigation POST::Store','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@store','2018-01-15 17:09:09','2018-01-15 17:09:09'),('907722046dc4caa4fa6b7d0ec3882963a0e40ab5','Appointment-Appointment GET::Edit','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@edit','2018-01-15 17:09:11','2018-01-15 17:09:11'),('91029119e2122c64ee6dc45455c304ecd39508b3','Appointment-Appointment POST::Store','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@store','2018-01-15 17:09:10','2018-01-15 17:09:10'),('937d5b7edcdbe8eecd0fecbd9db220a4486831b3','Pharmacy-Pharmacy GET::Activate','Pharmacy-Pharmacy','App\\Http\\Controllers\\Pharmacy\\PharmacyController@activate','2018-02-03 11:32:28','2018-02-03 11:32:28'),('93b4cc9fd3df15b3074ed39696c804c33d782009','Patient-ClinicalManagement GET::History','Patient-ClinicalManagement','App\\Http\\Controllers\\Patient\\ClinicalManagementController@history','2018-05-19 19:31:17','2018-05-19 19:31:17'),('9947237701b3e2e08f389fb42961948f8481ff4d','Role GET::Edit','User-Role','Uzzal\\Acl\\Http\\RoleController@edit','2018-01-15 17:01:13','2018-01-15 17:01:13'),('99d829b030fa992d75189fdc34c5db1c11f45d4a','Appointment-Appointment POST::Update','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@update','2018-01-15 17:09:11','2018-01-15 17:09:11'),('9b5890d6150e863e54adc3ba2fcb8f489e4ddaa6','Appointment-Appointment GET::Create','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@create','2018-01-15 17:09:11','2018-01-15 17:09:11'),('9c85cdba521a2dafd7a7c8e9af23016c9718c552','User-User GET::Activate','User-User','App\\Http\\Controllers\\User\\UserController@activate','2018-01-15 17:09:06','2018-01-15 17:09:06'),('a3086d6dbdcc7267892ee0e0370f7f3d986acfd2','Role GET::Index','User-Role','Uzzal\\Acl\\Http\\RoleController@index','2018-01-15 17:01:13','2018-01-15 17:01:13'),('a3aa481521fbce129d836e3132841c3386ad3411','Pharmacy-Pharmacy POST::Update','Pharmacy-Pharmacy','App\\Http\\Controllers\\Pharmacy\\PharmacyController@update','2018-02-03 11:32:28','2018-02-03 11:32:28'),('aba6f27bcb85a6ddfafd7827782d912021aac0be','Investigation-Investigation GET::Create','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@create','2018-01-15 17:09:10','2018-01-15 17:09:10'),('ae85a69bbf128046f36c541ec0f159cab32a4c52','Appointment-Appointment GET::CheckIn','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@checkIn','2018-01-15 17:09:11','2018-01-15 17:09:11'),('b2fb73fe2e8408f40204e7fd7a0b12321c0819d7','Appointment-Appointment GET::CheckedIn','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@checkedIn','2018-01-15 17:09:10','2018-01-15 17:09:10'),('bc58a6b79130434631adb0d4a40706f93f894707','Facility-FacilityUser GET::Index','Facility-FacilityUser','App\\Http\\Controllers\\Facility\\FacilityUserController@index','2018-01-15 17:09:09','2018-01-15 17:09:09'),('bc8c17f1d566d6d8dcd5044d320e64bceb44ce49','User-User GET::Destroy','User-User','App\\Http\\Controllers\\User\\UserController@destroy','2018-01-15 17:09:06','2018-01-15 17:09:06'),('be52bd0ffba8b93e43a9e13d9acfd7b03c584ae6','Disease-Disease GET::Create','Disease-Disease','App\\Http\\Controllers\\Disease\\DiseaseController@create','2018-01-15 17:09:07','2018-01-15 17:09:07'),('c27aa6936b63e1bd98cc7c756d85bfd26009fc3d','Investigation-Investigation GET::Edit','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@edit','2018-01-15 17:09:10','2018-01-15 17:09:10'),('c3ec459ee280417e4890b80a985d170d9b5f54a6','Resource GET::Create','User-Resource','Uzzal\\Acl\\Http\\ResourceController@create','2018-01-15 17:01:13','2018-01-15 17:01:13'),('c4f8fe2e6e2b9ca86281bddca6fdab502bd4d13b','Investigation-Investigation GET::Activate','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@activate','2018-01-15 17:09:09','2018-01-15 17:09:09'),('c5e26507f3bc0bfb3b0dfcc47e2478bb9618ec71','Drug-Drug GET::Edit','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@edit','2018-01-15 17:09:09','2018-01-15 17:09:09'),('c6d059ab811935a566f98ed3b084adab9f3e6742','Investigation-Investigation POST::Update','Investigation-Investigation','App\\Http\\Controllers\\Investigation\\InvestigationController@update','2018-01-15 17:09:10','2018-01-15 17:09:10'),('cadb4ae8a22d57dc1926084f4715fe7a2c61c8c6','Appointment-Appointment GET::ShowAppointmentTimes','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@showAppointmentTimes','2018-01-15 17:09:10','2018-01-15 17:09:10'),('cc35f16bb45fd7e945ccf1c4b2c5ae87970c6188','Appointment-Appointment GET::Canceled','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@canceled','2018-01-15 17:09:10','2018-01-15 17:09:10'),('cd878fad8f9be51d72f333e311a68e7e2e16ca1b','Drug-Drug GET::Index','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@index','2018-01-15 17:09:09','2018-01-15 17:09:09'),('cdf8b29d49ac4c5dddceffec6d03c1437d56d04f','User-User GET::Create','User-User','App\\Http\\Controllers\\User\\UserController@create','2018-01-15 17:09:06','2018-01-15 17:09:06'),('d0944f12517751077a7efca43c8d97aea52c49e4','Appointment-Appointment GET::Destroy','Appointment-Appointment','App\\Http\\Controllers\\Appointment\\AppointmentController@destroy','2018-01-15 17:09:11','2018-01-15 17:09:11'),('d49e54f8517151ee055075b22ca9945214766d52','Facility-Facility GET::Activate','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@activate','2018-01-15 17:09:08','2018-01-15 17:09:08'),('d5d2f9afb84562679a303fd6bd10d5ac0abc6b0a','Pharmacy-Pharmacy GET::Destroy','Pharmacy-Pharmacy','App\\Http\\Controllers\\Pharmacy\\PharmacyController@destroy','2018-02-03 11:32:28','2018-02-03 11:32:28'),('d85ef84219d0a23c506cf1d0aeddd3264cf5579c','Facility-Facility GET::Create','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@create','2018-01-15 17:09:08','2018-01-15 17:09:08'),('e2c82ca8a954b7cbc50f45502a585f4b35586c30','Drug-Drug GET::Destroy','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@destroy','2018-01-15 17:09:09','2018-01-15 17:09:09'),('e6e139e0827c06938d965507944321c14b3e8045','Facility-FacilityUser GET::Destroy','Facility-FacilityUser','App\\Http\\Controllers\\Facility\\FacilityUserController@destroy','2018-01-15 17:09:09','2018-01-15 17:09:09'),('e769f43b0a2c57fb6709356daa5fce5422baa2ac','Patient-ClinicalManagement POST::Store','Patient-ClinicalManagement','App\\Http\\Controllers\\Patient\\ClinicalManagementController@store','2018-01-15 17:09:11','2018-01-15 17:09:11'),('eb05927bd7a3cc13bbb4cd7fe0855624c4b3c864','Drug-Drug GET::Search','Drug-Drug','App\\Http\\Controllers\\Drug\\DrugController@search','2018-01-15 17:09:06','2018-01-15 17:09:06'),('ec801b996343e1eaff93e60a4290621bb0d9001d','Facility-FacilityUser POST::Store','Facility-FacilityUser','App\\Http\\Controllers\\Facility\\FacilityUserController@store','2018-01-15 17:09:09','2018-01-15 17:09:09'),('ed2bf49c97e061e99cc3616b65518d49737b8aba','Facility-Facility GET::Show','Facility-Facility','App\\Http\\Controllers\\Facility\\FacilityController@show','2018-01-15 17:09:08','2018-01-15 17:09:08'),('f5ac52e6c79edb2d2d6c79949c815162a23e9637','Role POST::Store','User-Role','Uzzal\\Acl\\Http\\RoleController@store','2018-01-15 17:01:13','2018-01-15 17:01:13');
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Developer','2018-01-15 17:01:13','2018-01-15 17:01:13'),(2,'Default','2018-01-15 17:01:13','2018-01-15 17:01:13'),(3,'PHARMACY','2018-02-03 11:52:08','2018-02-03 11:52:08');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_results`
--

DROP TABLE IF EXISTS `test_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_results` (
  `test_result_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(10) unsigned NOT NULL,
  `pharmacy_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `result` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`test_result_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_results`
--

LOCK TABLES `test_results` WRITE;
/*!40000 ALTER TABLE `test_results` DISABLE KEYS */;
INSERT INTO `test_results` VALUES (1,2,1,'BLOOD_SUGAR','2018-06-02 22:25:38','{\"afterBreakfast\":\"0\",\"fasting\":\"0\",\"random\":\"10\"}','2018-06-02 22:25:40','2018-06-02 22:25:40');
/*!40000 ALTER TABLE `test_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1),(2,1),(2,2),(4,3);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Rahat Bashir','rahat.bashir@yano.tech','$2y$10$bO2mnXfT5ae97DgSqgrDEOltdygtpZBbdtAVtfYgJ0ByRfEj458j.',1,NULL,NULL,'2018-01-15 17:26:47'),(2,'Mahabubul Hasan','mahabub@yano.tech','$2y$10$QbQck.4flysEmQlD7zIUUOG83Lj1f.jj9O3cUm7uoPdXLrPUSPd/K',1,'d8VHkVQPuySqxnukInLRXKQUkY8QNoBvk5UuAqddcXqcgr7ZVPmx51jB4Tw5',NULL,'2018-01-15 17:26:49'),(4,'Anurag Pharmacy Ltd.','anurag@example.com','$2y$10$bO2mnXfT5ae97DgSqgrDEOltdygtpZBbdtAVtfYgJ0ByRfEj458j.',1,NULL,'2018-02-03 12:02:59','2018-02-03 15:59:14');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-18 15:11:39
