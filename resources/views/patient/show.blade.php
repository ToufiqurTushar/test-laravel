@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/patient">Patients</a></li>
                <li class="active">Show</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success" role="alert">{{ $msg }}</div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading',[
                            'form_title'=>'Patient Details',
                            'links' => [
                                ['href' => url('patient/edit',$row->patient_id), 'link_name' => 'Edit']
                            ],
                            'back' => true
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <legend>{{ $row->name }}</legend>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab" aria-expanded="false">General</a></li>
                                    <li class=""><a href="#demographic" data-toggle="tab" aria-expanded="false">Demographic</a></li>
                                    <li class=""><a href="#contact" data-toggle="tab" aria-expanded="false">Contact</a></li>
                                    <li class=""><a href="#blood-pressure" data-toggle="tab" aria-expanded="false">Blood Pressure</a></li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade active in" id="general">
                                        <dl class="dl-horizontal">
                                            <dt>Name</dt>
                                            <dd>{{ $row->name }}</dd>
                                            <dt>National ID</dt>
                                            <dd>{{ $row->national_id }}</dd>
                                            <dt>Insurance Company</dt>
                                            <dd>{{ $row->insurance_company }}</dd>
                                            <dt>Policy Number</dt>
                                            <dd>{{ $row->insurance_policy_number }}</dd>
                                            <dt>Record Created at</dt>
                                            <dd>{{ $row->created_at ? date('F d, Y h:i:s a', strtotime($row->created_at)) : 'Not available' }}</dd>
                                            <dt>Record last Updated at</dt>
                                            <dd>{{ $row->updated_at ? date('F d, Y h:i:s a',strtotime($row->updated_at)) : 'Not available' }}</dd>
                                        </dl>
                                    </div>
                                    <div class="tab-pane fade" id="demographic">
                                        <dl class="dl-horizontal">
                                            <dt>Gender</dt>
                                            <dd>{{ $row->gender }}</dd>
                                            <dt>Religion</dt>
                                            <dd>{{ $row->religion }}</dd>
                                            <dt>Civil Status</dt>
                                            <dd>{{ $row->civil_status }}</dd>
                                            <dt>Date of Birth</dt>
                                            <dd>{{ $row->dob ? date('F d, Y', strtotime($row->dob)) : 'Not available' }}</dd>
                                            <dt>Place of Birth</dt>
                                            <dd>{{ $row->birth_place }}</dd>
                                            <dt>Blood Group</dt>
                                            <dd>{{ $row->blood_group }}</dd>
                                        </dl>
                                    </div>
                                    <div class="tab-pane fade" id="contact">
                                        <dl class="dl-horizontal">
                                            <dt>Mobile Number</dt>
                                            <dd>{{ $row->mobile }}</dd>
                                            <dt>Phone</dt>
                                            <dd>{{ $row->phone }}</dd>
                                            <dt>Email</dt>
                                            <dd>{{ $row->email }}</dd>
                                            <dt>Address</dt>
                                            <dd>{{ $row->address }}</dd>
                                        </dl>
                                    </div>
                                    <div class="tab-pane fade" id="blood-pressure">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Systolic</th>
                                                <th>Diastolic</th>
                                                <th>Pulse</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse($row->bloodPressures as $bp)
                                            <tr>
                                                <td>{{ $bp->systolic }}</td>
                                                <td>{{ $bp->diastolic }}</td>
                                                <td>{{ $bp->pulse }}</td>
                                                <td>{{ $bp->date }}</td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="4">Nothing Found !</td>
                                            </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/ncd-style.css') }}">
@endsection