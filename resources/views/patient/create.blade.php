@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/patient">Patients</a></li>
                <li class="active">Register</li>
            </ol>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Register New Patient',
                            'links' => [
                                ['href' => url('patient'), 'link_name' => 'Patients']
                            ],
                            'back' => true
                        ])
                    </div>

                    <div class="panel-body">
                        @include('shared.errors')

                        <form class="form-horizontal" method="POST" action="{{ url('patient') }}">
                            {{ csrf_field() }}

                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">Name<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="mobile">Contact Number</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="mobile" placeholder="mobile..." name="mobile" value="{{ old('mobile') }}">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="phone..." value="{{ old('phone') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Email Address</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="addr">Address<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="addr" name="address" placeholder="physical address..." value="{{ old('address') }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="religion">Religion<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" id="religion" class="form-control" list="religion_list" name="religion" placeholder="write religion..." value="{{ old('religion') }}" required>
                                        <datalist id="religion_list">
                                            <option value="Islam">
                                            <option value="Christianity">
                                            <option value="Buddhism">
                                            <option value="Hinduism">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="civil_status">Civil Status<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="civil_status" list="civil_status_list" name="civil_status" placeholder="married/single..." value="{{ old('civil_status') }}" required>
                                        <datalist id="civil_status_list">
                                            <option value="Married">
                                            <option value="Single">
                                            <option value="Widowed">
                                            <option value="Divorced">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="sex">Sex<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="sex" list="sex_list" name="sex" placeholder="male/female/trans..." value="{{ old('sex') }}" required>
                                        <datalist id="sex_list">
                                            <option value="Male">
                                            <option value="Female">
                                            <option value="Trans-gender">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="dob">Date of Birth</label>
                                    <div class="col-md-7">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" id="dob" name="dob" placeholder="select date..." value="{{ old('dob') }}">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="place">Place of Birth<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="place" name="birth_place" value="{{ old('birth_place') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="blood_grp">Blood Group<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="blood_grp" list="blood_grp_list" name="blood_group" placeholder="write blood group..." value="{{ old('blood_group') }}">
                                        <datalist id="blood_grp_list">
                                            <option value="A+">
                                            <option value="B+">
                                            <option value="O+">
                                            <option value="B-">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="nid">National ID</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="nid" name="national_id" value="{{ old('national_id') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="insurance">Insurance Company</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="insurance" name="insurance_company" placeholder="name of insurance company..." value="{{ old('insurance_company') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="policyno">Insurance Policy Number</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="policyno" name="insurance_policy_number" placeholder="policy number..." value="{{ old('insurance_policy_number') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-7 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span>Save</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
@endsection

@section('script')
    <script src="{{ asset('lib/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function () {
            $('.date').datepicker({
                format:'yyyy-mm-dd',
                todayHighlight: true,
                clearBtn:true,
                todayBtn:true,
                autoclose:true,
                daysOfWeekHighlighted:"5,6"
            });
        });
    </script>
@endsection