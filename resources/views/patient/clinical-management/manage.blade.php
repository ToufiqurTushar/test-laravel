@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/patient">Patients</a></li>
                <li class="active">Clinical Management</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-1">
                        <div id="qrcode"></div>
                    </div>
                    <div class="col-sm-5">
                        <div>Name: <strong>{{$patient->name}}</strong></div>
                        <div>Age: <strong>{{age($patient->dob)}}</strong> Years (Approx.)</div>
                        <div>Blood Group: <strong>{{$patient->blood_group}}</strong></div>
                        <div>Sex: <strong>{{$patient->sex}}</strong></div>
                        <input id="qrinfo" type="hidden" value="{{qrcode_info($patient)}}">
                    </div>
                </div>

                <div id="clinicalManagement" class="col-sm-12">
                    <clinical-management :patient_id="{{$patient->patient_id}}" :gender="'{{strtolower($patient->sex)}}'" />
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('js/qrcode.min.js')}}"></script>
    <script type="text/javascript">
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            width: 80,
            height: 80
        });
        qrcode.makeCode(document.getElementById('qrinfo').value);
    </script>
@endsection