@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/patient">Patients</a></li>
                <li class="active">Clinical Management</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif
            <div class="">
                <div id="clinicalManagement" >
                    <clinical-management :patient_id="1" :gender="'female'" />
                </div>
            </div>
        </div>
    </div>
@endsection