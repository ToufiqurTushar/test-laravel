@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/facility">Facility</a></li>
                <li class="active">Users</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    @inject('facility','App\Repositories\FacilityRepository')
                    <?php $facility_details = $facility->getItem($facility_id); ?>
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Permitted Users of '.$facility_details->name,
                            'badge'=> $rows->count().' of '.$rows->total(),
                            'links' => [
                                has_access('Facility\FacilityUserController@create') ? ['href' => url('facility/'.$facility_id.'/user/add'), 'link_name' => '+ Add New'] : []
                            ],
                            'back' => false
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if(count($rows)>0)
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>User ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Assign Date</th>
                                            <th>Operations</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($rows as $row)
                                        <tr>
                                            <td>{{ $row->user_id }}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->is_active ? 'Active' : '' }}</td>
                                            <td>{{ date_to_day($row->assign_date) }}</td>
                                            <td>
                                                @if(has_access('Facility\FacilityUserController@destroy'))
                                                <a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Revoke access" onclick="return confirm('Are you sure you want to revoke access permission for this user?')" href="{{url('facility/user/revoke', $row->facility_user_id)}}"><span class="glyphicon glyphicon-remove"></span></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <tr><td colspan="6"><div class="alert alert-info">No assigned user found!</div></div></td></tr>
                            @endif
                            {{ $rows->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
