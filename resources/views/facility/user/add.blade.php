@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/facility">Facility</a></li>
                <li><a href="/facility/{{ $facility_id }}/user">User</a></li>
                <li class="active">Add</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    @inject('facility','App\Repositories\FacilityRepository')
                    <?php $facility_details = $facility->getItem($facility_id); ?>
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Assign new Users to '.$facility_details->name,
                            'badge'=> $rows->count().' of '.$rows->total(),
                            'links' => [],
                            'back' => true
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if(count($rows)>0)
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>User ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            {{--<th>Assign Date</th>--}}
                                            <th>Operations</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($rows as $row)
                                        <tr>
                                            <td>{{ $row->user_id }}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->is_active ? 'Active' : '' }}</td>
                                            <td>
                                                @if(has_access('Facility\FacilityUserController@store'))
                                                <a class="btn btn-xs btn-primary grant_access" href="{{url('facility/user/add?fid='.$facility_id.'&uid='.$row->user_id)}}"><span class="glyphicon glyphicon-plus"></span></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <tr><td colspan="6"><div class="alert alert-info">No assignable user found!</div></div></td></tr>
                            @endif
                            {{ $rows->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $('.grant_access').click(function (e) {
                if(confirm('Are you sure you want to grant access permission to this user?')){
                    e.preventDefault();
                    var href = this.href;
                    var parts = href.split('?');
                    var url = parts[0];
                    var params = parts[1].split('&');
                    var n = params.length;
                    var name_val = '';
                    var form = $('<form action="'+ url +'" method="post">{{ csrf_field() }}</form>');
                    for(var i = 0; i < n; i++){
                        name_val = params[i].split('=');
                        $('<input>').attr({type:'hidden', name:name_val[0], value:name_val[1]}).appendTo(form);
                    }
                    $(form).appendTo('body').submit();
                }
            });
        });
    </script>
@endsection
