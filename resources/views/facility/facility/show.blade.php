@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/facility">Facilities</a></li>
                <li class="active">Show</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success" role="alert">{{ $msg }}</div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading',[
                            'form_title'=>'Facility Details',
                            'links' => [
                                has_access('Facility\FacilityController@edit') ? ['href' => url('facility/edit', $row->facility_id), 'link_name' => 'Edit'] : []
                            ],
                            'back' => true
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <legend>{{ $row->name }}</legend>
                                <dl class="dl-horizontal">
                                    <dt>Name</dt>
                                    <dd>{{ $row->name }}</dd>
                                    <dt>Contact Person</dt>
                                    <dd>{{ $row->contact_person }}</dd>
                                    <dt>Email</dt>
                                    <dd>{{ $row->email }}</dd>
                                    <dt>Mobile</dt>
                                    <dd>{{ $row->mobile }}</dd>
                                    <dt>Other Phone</dt>
                                    <dd>{{ $row->phone }}</dd>
                                    <dt>Address</dt>
                                    <dd>{{ $row->address }}</dd>
                                    <dt>Specialization</dt>
                                    <dd>{{ $row->specialization }}</dd>
                                    <dt>Engaged with Project</dt>
                                    <dd>{{ $row->engagement_date ? date('F d, Y h:i:s a', strtotime($row->engagement_date)) : 'Date not available' }}</dd>
                                    <dt>Record Created at</dt>
                                    <dd>{{ $row->created_at ? date('F d, Y h:i:s a', strtotime($row->created_at)) : 'Date not available' }}</dd>
                                    <dt>Record last Updated at</dt>
                                    <dd>{{ $row->updated_at ? date('F d, Y h:i:s a',strtotime($row->updated_at)) : 'Date not available' }}</dd>
                                </dl>
                                <a class="btn btn-primary btn-xs pull-right" href="{{ url('facility') }}"><span class="glyphicon glyphicon-ok"></span> Okay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/ncd-style.css') }}">
@endsection