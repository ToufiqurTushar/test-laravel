@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/facility">Facilities</a></li>
                <li class="active">Edit</li>
            </ol>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Edit Facility',
                            'links' => [],
                            'back' => true
                        ])
                    </div>

                    <div class="panel-body">
                        @include('shared.errors')

                        <form class="form-horizontal" method="POST" action="{{ url('facility', $row->facility_id) }}">
                            {{ csrf_field() }}

                            <fieldset>
                                {{--<legend>Facility</legend>--}}
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">Name<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $row->name) }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="contact">Contact Person<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="contact" name="contact_person" value="{{ old('contact_person', $row->contact_person) }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Email Address</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $row->email) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="mobile">Contact Number</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="mobile" placeholder="Mobile number..." name="mobile" value="{{ old('mobile', $row->mobile) }}">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone number..." value="{{ old('phone', $row->phone) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="addr">Address</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="addr" name="address" value="{{ old('address', $row->address) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="special">Specialized In<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="special" name="specialization" value="{{ old('specialization', $row->specialization) }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="engage_date">Engaged On</label>
                                    <div class="col-md-7">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" id="engage_date" name="engagement_date" value="{{ old('engagement_date', $row->engagement_date) }}">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-7 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <span class="glyphicon glyphicon-refresh"></span>
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
@endsection

@section('script')
    <script src="{{ asset('lib/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function () {
            $('.date').datepicker({
                format:'yyyy-mm-dd',
                todayHighlight: true,
                clearBtn:true,
                todayBtn:true,
                autoclose:true,
                daysOfWeekHighlighted:"5,6"
            });
        });
    </script>
@endsection