@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/disease">Diseases</a></li>
                <li class="active">Add</li>
            </ol>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Add Disease',
                            'links' => [['href' => '/disease', 'link_name' => 'Diseases']],
                            'back' => true
                        ])
                    </div>

                    <div class="panel-body">
                        @include('shared.errors')

                        <form class="form-horizontal" method="POST" action="{{ url('disease') }}">
                            {{ csrf_field() }}

                            <fieldset>
                                {{--<legend>Disease</legend>--}}
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">Name<small class="text-danger">*</small></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="description">Description</label>
                                    <div class="col-md-7">
                                        <textarea rows="3" cols="7" class="form-control" id="description" name="description">{{ old('description') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-7 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <span class="glyphicon glyphicon-floppy-disk"></span>
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection