@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/disease">Diseases</a></li>
                <li class="active">Show</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success" role="alert">{{ $msg }}</div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading',[
                            'form_title'=>'Disease Details',
                            'links' => [
                                has_access('Disease\DiseaseController@edit') ? ['href' => url('disease/edit', $row->disease_id), 'link_name' => 'Edit'] : []
                            ],
                            'back' => true
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <legend>{{ $row->name }}</legend>
                                <p>{{ $row->description }}</p>
                                <a class="btn btn-primary btn-xs pull-right" href="{{ url('disease') }}"><span class="glyphicon glyphicon-ok"></span> Okay</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection