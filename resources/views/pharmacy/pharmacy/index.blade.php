@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/pharmacy">Pharmacy</a></li>
                <li class="active">List</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    {{--<div class="panel-heading">
                        @include('shared.search-criteria',[
                            'brand_name' => true,
                            'generic_name' => true,
                            'dosage_form' => true,
                            'state' => true
                        ])
                    </div>--}}
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Pharmacy',
                            'badge'=> $rows->count() . ' of ' . $rows->total(),
                            'links' => [
                                has_access('Pharmacy\PharmacyController@create') ? ['href' => '/pharmacy/add', 'link_name' => '+ Add New'] : []
                            ],
                            'back' => false
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pharmacy Name</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $counter = 1; ?>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{ $counter++ }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->address }}</td>
                                        <td>{{ $row->phone }}</td>
                                        <td>{{ @nullsafe($row->user->email) }}</td>
                                        <td>
                                            @if(has_access('Pharmacy\PharmacyController@edit'))
                                            <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit" href="{{url('pharmacy/edit', $row->pharmacy_id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                                            @endif

                                            @php if($row->is_active){$icn = 'glyphicon-minus'; $state = 'false'; $ttl = 'Deactivate'; $cls = 'btn-warning';}else{$icn = 'glyphicon-ok'; $state = 'true'; $ttl = 'Activate'; $cls = 'btn-info';} @endphp

                                            @if(has_access('Pharmacy\PharmacyController@activate'))
                                            <a class="btn btn-xs btn-xs {{ $cls }}" data-toggle="tooltip" title="{{ $ttl }}" href="{{url('pharmacy/activate/'.$row->pharmacy_id.'/'.$state)}}"><span class="glyphicon {{ $icn }}"></span></a>
                                            @endif

                                            @if(has_access('Pharmacy\PharmacyController@destroy'))
                                            <a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Remove" onclick="return confirm('Are you sure you want to remove this drug?')" href="{{url('pharmacy/remove', $row->pharmacy_id)}}"><span class="glyphicon glyphicon-trash"></span></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $rows->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection