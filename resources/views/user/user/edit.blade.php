@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/user">Users</a></li>
                <li class="active">Edit</li>
            </ol>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Edit User',
                            'links' => [],
                            'back' => true
                        ])
                    </div>

                    <div class="panel-body">
                        @include('shared.errors')
                        <form class="form-horizontal" method="post" action="{{ url('user', $row->user_id) }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $row->name) }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $row->email) }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Role</label>
                                <div class="col-md-6">
                                    @php
                                        $user_roles = array_flatten($row->roles()->get(['role_id'])->toArray());
                                    @endphp
                                    @foreach($roles as $role)
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="role_id[]" value="{{$role->role_id}}" {{(in_array($role->role_id, old('role_id',$user_roles)))?'checked=checked':''}}>
                                                {{ $role->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <span class="glyphicon glyphicon-refresh"></span>
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection