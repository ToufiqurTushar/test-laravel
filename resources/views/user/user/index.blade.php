@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/user">Users</a></li>
                <li class="active">Register</li>
            </ol>
            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.search-criteria',[
                            'name' => true,
                            'email' => true,
                            'state' => true
                        ])
                    </div>
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Registered Users',
                            'badge' => $rows->count() . ' of ' . $rows->total(),
                            'links' => [
                                has_access('User\UserController@create') ? ['href' => '/user/register', 'link_name' => '+ Register New'] : []
                            ],
                            'back' => false
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="table table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Operations</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $counter = 1; ?>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{ $counter++ }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->email }}</td>
                                        <td>{{ role_names($row->roles) }}</td>
                                        <td>
                                            @if(has_access('User\UserController@edit'))
                                            <a class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit" href="{{ url('user/edit', $row->user_id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                                            @endif

                                            @php if($row->is_active){$icn = 'glyphicon-minus'; $state = 'false'; $ttl = 'Deactivate'; $cls = 'btn-warning';}else{$icn = 'glyphicon-ok'; $state = 'true'; $ttl = 'Activate'; $cls = 'btn-info';} @endphp

                                            @if(has_access('User\UserController@activate'))
                                            <a class="btn btn-info btn-xs {{ $cls }}" data-toggle="tooltip" title="{{ $ttl }}" href="{{ url('user/activate/'.$row->user_id.'/'.$state) }}"><span class="glyphicon {{ $icn }}"></span></a>
                                            @endif

                                            @if(has_access('User\UserController@destroy'))
                                            <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="Remove" onclick="return confirm('Are you sure you want to remove this user?')" href="{{ url('user/remove', $row->user_id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $rows->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection