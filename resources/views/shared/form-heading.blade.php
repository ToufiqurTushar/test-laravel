<div class="pull-left panel-title">{{ $form_title }} <span class="badge">@if(@isset($badge)) {{ $badge }} @endif</span></div>
<div class="pull-right">
    @if(count($links))
        @foreach($links as $link)
            @if($link)
            <a class="btn btn-primary btn-xs" href="{{ $link['href'] }}">{{ $link['link_name'] }}</a>
            @endif
        @endforeach
    @endif

    @if($back)
        <a class="btn btn-primary btn-xs" onclick="window.history.back()">&laquo; Back</a>
    @endif
</div>
<div class="clearfix"></div>