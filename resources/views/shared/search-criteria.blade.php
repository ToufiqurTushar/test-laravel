<form class="form-inline" action="" method="get">
    @if(@isset($brand_name))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="brand_name" placeholder="brand name..." value="">
    </div>
    @endif

    @if(@isset($generic_name))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="generic_name" placeholder="generic name..." value="">
    </div>
    @endif

    @if(@isset($dosage_form))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="dosage_form" placeholder="tablet/capsule/inj..." value="">
    </div>
    @endif

    @if(@isset($name))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="name" placeholder="name..." value="">
    </div>
    @endif

    @if(@isset($email))
        <div class="form-group">
            <input type="text" class="form-control input-sm" name="email" placeholder="email address..." value="">
        </div>
    @endif

    @if(@isset($contact_person))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="contact_person" placeholder="contact person..." value="">
    </div>
    @endif

    @if(@isset($specialization))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="specialization" placeholder="special in..." value="">
    </div>
    @endif

    @if(@isset($sex))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="sex" placeholder="sex..." value="">
    </div>
    @endif

    @if(@isset($blood_group))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="blood_group" placeholder="blood group..." value="">
    </div>
    @endif

    @if(@isset($mobile))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="mobile" placeholder="mobile..." value="">
    </div>
    @endif

    @if(@isset($national_id))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="national_id" placeholder="national id..." value="">
    </div>
    @endif

    @if(@isset($patient_name))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="patient_name" placeholder="patient name..." value="">
    </div>
    @endif

    @if(@isset($patient_mobile))
        <div class="form-group">
            <input type="text" class="form-control input-sm" name="patient_mobile" placeholder="patient mobile..." value="">
        </div>
    @endif

    @if(@isset($facilitator_name))
    <div class="form-group">
        <input type="text" class="form-control input-sm" name="facilitator_name" placeholder="facilitator name..." value="">
    </div>
    @endif

    @if(@isset($appointment_date))
    <div class="input-group date">
        <input type="text" class="form-control input-sm" name="appointment_date" placeholder="appointment date..." value="">
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </div>
    </div>
    @endif

    @if(@isset($state))
    <div class="checkbox"><label><input type="checkbox" name="state" value="deactivated"> Deactivated</label></div>
    @endif

    @if(@isset($cancel_state))
    <div class="checkbox"><label><input type="checkbox" name="is_canceled" value="canceled"> Canceled</label></div>
    @endif

    <button type="submit" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-search"></span> Search</button>
</form>

@if($r = array_filter(request()->all()))
    <em>Criteria:</em>
    @foreach($r as $k => $v)
        <span class="label label-default">{{ $k .': '. $v}}</span>
    @endforeach
@endif