@if(count($rows) > 0)
<h5 class="h5"></h5>
<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th class="small">Date</th>
            <th class="small">Patient</th>
            <th class="small">Time</th>
        </tr>
    </thead>
    <tbody>
    @php $d = null; @endphp
        @foreach($rows as $row)
            <tr>
                <td class="small">{{ $d == $row->appointment_date ? '' : date('F d, \'y', strtotime($row->appointment_date)) }}</td>
                <td class="small">{{ $row->patient_name }}</td>
                <td class="small">{{ $row->appointment_time }}</td>
            </tr>
            @php $d = $row->appointment_date; @endphp
        @endforeach
    </tbody>
</table>
@else
    <div class="alert alert-info">No appointment found</div>
@endif