<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ConnectNCD</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
    <style type="text/css">
        .navbar-brand{
            color:#005aab !important;
        }
        .navbar-brand span{
            color:#ed1b24;
        }
    </style>
    @yield('link')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
        ;
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        Connected<span>NCD</span>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if(has_group_access([
                            'User-Role',
                            'User-User',
                            'User-Resource',
                            'Facility-Facility',
                            'Disease-Disease',
                            'Drug-Drug',
                            'Investigation-Investigation',
                            'Pharmacy-Pharmacy'
                            ]))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="dropdown-header">Manage Access</li>
                                <li><a href="{{ url('role') }}">Roles</a></li>
                                <li><a href="{{ url('role/create') }}">Add Role</a></li>
                                <li><a href="{{ url('resource') }}">Resources</a></li>
                                <li><a href="{{ url('resource/create') }}">Add Resource</a></li>
                                <li><a href="{{ url('user') }}">Users</a></li>
                                <li><a href="{{ url('user/register') }}">Register User</a></li>
                                <li><hr class="divider"></li>
                                {{--<li class="dropdown-header">Manage Facilities</li>--}}
                                <li><a href="{{ url('facility') }}">Clinical Facilities</a></li>
                                <li><a href="{{ url('facility/add') }}">Add Facility</a></li>
                                <li><hr class="divider"></li>
                                {{--<li class="dropdown-header">Manage Diseases</li>--}}
                                <li><a href="{{ url('disease') }}">Diseases</a></li>
                                <li><a href="{{ url('disease/add') }}">Add Disease</a></li>
                                <li><hr class="divider"></li>
                                {{--<li class="dropdown-header">Manage Drugs</li>--}}
                                <li><a href="{{ url('drug') }}">Allowed Drugs</a></li>
                                <li><a href="{{ url('drug/add') }}">Add Drug</a></li>
                                <li><hr class="divider"></li>
                                {{--<li class="dropdown-header">Manage Investigations</li>--}}
                                <li><a href="{{ url('investigation') }}">Investigations</a></li>
                                <li><a href="{{ url('investigation/add') }}">Add Investigation</a></li>
                                <li><hr class="divider"></li>
                                <li><a href="{{ url('pharmacy') }}">Pharmacy List</a></li>
                                <li><a href="{{ url('pharmacy/add') }}">Add Pharmacy</a></li>
                            </ul>
                        </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Manage Patients <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                {{--<li class="dropdown-header">Manage Patients</li>--}}
                                @if(has_access('Patient\PatientController@index'))
                                <li><a href="{{ url('patient') }}">Patients</a></li>
                                @endif
                                @if(has_access('Patient\PatientController@create'))
                                <li><a href="{{ url('patient/register') }}">Register Patient</a></li>
                                @endif
                                <li><a href="{{ url('clinical-management') }}">Clinical Management</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Manage Appointments <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                {{--<li class="dropdown-header">Manage Appointments</li>--}}
                                @if(has_access('Appointment\AppointmentController@index'))
                                <li><a href="{{ url('appointment') }}">Appointments</a></li>
                                @endif
                                @if(has_access('Appointment\AppointmentController@create'))
                                <li><a href="{{ url('appointment/add') }}">Make Appointment</a></li>
                                @endif
                                @if(has_access('Appointment\AppointmentController@checkedIn'))
                                <li><a href="{{ url('appointment/checked-in') }}">Checked-in Patients</a></li>
                                @endif
                                @if(has_access('Appointment\AppointmentController@canceled'))
                                <li><a href="{{ url('appointment/checked-in') }}">Canceled Appointments</a></li>
                                @endif
                            </ul>
                        </li>

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}?v=1.0"></script>
    @yield('script')
</body>
</html>
