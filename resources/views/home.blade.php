@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="flex-container">
                        @if(has_group_access([
                            'Facility-Facility',
                            'Disease-Disease',
                            'Drug-Drug',
                            'Investigation-Investigation',
                            'Pharmacy-Pharmacy'
]                       ))
                        <div>
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('facility') }}">Clinical Facilities</a>
                            </div>
                        </div>
                        <div>
                            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('disease') }}">Diseases</a>
                            </div>
                        </div>
                        <div>
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('drug') }}">Allowed Drugs</a>
                            </div>
                        </div>
                        <div>
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('investigation') }}">Investigations</a>
                            </div>
                        </div>
                        <div>
                            <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('pharmacy') }}">Pharmacy List</a>
                            </div>
                        </div>
                        @endif
                        @if(has_access('Patient\PatientController@index'))
                        <div>
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('patient') }}">Patients</a>
                            </div>
                        </div>
                        @endif
                        @if(has_access('Patient\PatientController@create'))
                        <div>
                            <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('patient/register') }}">Register Patients</a>
                            </div>
                        </div>
                        @endif
                        <div>
                            <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('clinical-management') }}">Clinical Management</a>
                            </div>
                        </div>
                        @if(has_access('Appointment\AppointmentController@index'))
                        <div>
                            <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('appointment') }}">Appointments</a>
                            </div>
                        </div>
                        @endif
                        @if(has_access('Appointment\AppointmentController@create'))
                        <div>
                            <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('appointment/add') }}">Make Appointment</a>
                            </div>
                        </div>
                        @endif
                        @if(has_access('Appointment\AppointmentController@checkedIn'))
                        <div>
                            <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('appointment/checked-in') }}">Checked In Patients</a>
                            </div>
                        </div>
                        @endif
                        @if(has_access('Appointment\AppointmentController@canceled'))
                        <div>
                            <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                            <div>
                                <a href="{{ url('appointment/checked-in') }}">Canceled Patients</a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('link')
    <style>
        .flex-container{
            display:flex;
            flex-wrap: wrap;
            height: 200px;
            justify-content: center;
            margin: 30px 0;
        }
        .flex-container > div{
            border:1px solid #ddd;
            width: 100px;
            margin: 5px;
            text-align: center;
            padding: 10px;
            border-radius: 3px;
            box-shadow: inset 0 1px 0 rgba(255,255,255,.15), 0 1px 5px rgba(0,0,0,.075);
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
        }

        .flex-container > div:hover{
            background: #ddd;
        }

        .flex-container > div > span{
            color: #1f648b;
            font-size: 25pt;
        }
    </style>
@endsection