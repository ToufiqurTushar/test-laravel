@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/drug">Drugs</a></li>
                <li class="active">Add</li>
            </ol>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Add Drug',
                            'links' => [['href' => '/drug', 'link_name' => 'Drugs']],
                            'back' => true
                        ])
                    </div>

                    <div class="panel-body">
                        @include('shared.errors')

                        <form class="form-horizontal" method="POST" action="{{ url('drug') }}">
                            {{ csrf_field() }}

                            <fieldset>
                                {{--<legend>Drug</legend>--}}
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="brand">Brand Name<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="brand" name="brand_name" value="{{ old('brand_name') }}" required autofocus maxlength="128">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="generic">Generic Name<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="generic" name="generic_name" value="{{ old('generic_name') }}" required maxlength="128">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="dosage_form">Dosage Form<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <select class="form-control" id="dosage_form" name="dosage_form" required>
                                            <option value="">&lt;Select please... &gt;</option>
                                            <option value="Tablet" {{ old('dosage_form') == 'Tablet' ? 'selected' : '' }}>Tablet</option>
                                            <option value="Capsule" {{ old('dosage_form') == 'Capsule' ? 'selected' : '' }}>Capsule</option>
                                            <option value="Injection" {{ old('dosage_form') == 'Injection' ? 'selected' : '' }}>Injection</option>
                                            <option value="Syrup" {{ old('dosage_form') == 'Syrup' ? 'selected' : '' }}>Syrup</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="dosage_unit">Dosage Unit<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <select class="form-control" id="dosage_unit" name="dosage_unit" required>
                                            <option value="">&lt;Select please... &gt;</option>
                                            <option value="mg" {{ old('dosage_unit') == 'mg' ? 'selected' : '' }}>mg</option>
                                            <option value="ml" {{ old('dosage_unit') == 'ml' ? 'selected' : '' }}>ml</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="age">Appropriate For<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <select class="form-control" id="age" name="age_group" required>
                                            <option value="">&lt;Select please... &gt;</option>
                                            <option value="Adult" {{ old('age_group') == 'Adult' ? 'selected' : '' }}>Adult</option>
                                            <option value="Children" {{ old('dosage_form') == 'Children' ? 'selected' : '' }}>Children</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="administration">Administration Route<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <select class="form-control" id="administration_route" name="administration_route" required>
                                            <option value="" selected>&lt;Select please... &gt;</option>
                                            <option value="Oral" {{ old('administration_route') == 'Oral' ? 'selected' : '' }}>Oral</option>
                                            <option value="Sublinqual" {{ old('administration_route') == 'Sublinqual' ? 'selected' : '' }}>Sublingual</option>
                                            <option value="Buccal" {{ old('administration_route') == 'Buccal' ? 'selected' : '' }}>Buccal</option>
                                            <option value="Rectal" {{ old('administration_route') == 'Rectal' ? 'selected' : '' }}>Rectal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="description">Description</label>
                                    <div class="col-md-7">
                                        <textarea rows="3" cols="7" class="form-control" id="description" name="description">{{ old('description') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="indication">Indication<span class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <textarea rows="3" cols="7" class="form-control" id="indication" name="indication">{{ old('indication') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-7 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <span class="glyphicon glyphicon-floppy-disk"></span>
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection