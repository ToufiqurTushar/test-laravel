@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/drug">Drugs</a></li>
                <li class="active">Show</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success" role="alert">{{ $msg }}</div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading',[
                            'form_title'=>'Drug Details',
                            'links' => [
                                has_access('Drug\DrugController@edit') ? ['href' => url('drug/edit', $row->drug_id), 'link_name' => 'Edit'] : []
                            ],
                            'back' => true
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <legend>{{ $row->brand_name }}</legend>
                                <dl class="dl-horizontal">
                                    <dt>Brand Name</dt>
                                    <dd>{{ $row->brand_name }}</dd>
                                    <dt>Generic Name</dt>
                                    <dd>{{ $row->generic_name }}</dd>
                                    <dt>Dosage Form</dt>
                                    <dd>{{ $row->dosage_form }}</dd>
                                    <dt>Dosage Unit</dt>
                                    <dd>{{ $row->dosage_unit }}</dd>
                                    <dt>Age Group</dt>
                                    <dd>{{ $row->age_group }}</dd>
                                    <dt>Administration Route</dt>
                                    <dd>{{ $row->administration_route }}</dd>
                                    <dt>Description</dt>
                                    <dd>{{ $row->description }}</dd>
                                    <dt>Indication</dt>
                                    <dd>{{ $row->indication }}</dd>
                                    <dt>Record Created at</dt>
                                    <dd>{{ $row->created_at ? date('F d, Y h:i:s a', strtotime($row->created_at)) : 'Not Available' }}</dd>
                                    <dt>Record last Updated at</dt>
                                    <dd>{{ $row->updated_at ? date('F d, Y h:i:s a',strtotime($row->updated_at)) : 'Not Available' }}</dd>
                                </dl>
                                <a class="btn btn-primary btn-xs pull-right" href="{{ url('drug') }}"><span class="glyphicon glyphicon-ok"></span> Okay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/ncd-style.css') }}">
@endsection