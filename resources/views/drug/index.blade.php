@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/drug">Drugs</a></li>
                <li class="active">List</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.search-criteria',[
                            'brand_name' => true,
                            'generic_name' => true,
                            'dosage_form' => true,
                            'state' => true
                        ])
                    </div>
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Drugs',
                            'badge'=> $rows->count() . ' of ' . $rows->total(),
                            'links' => [
                                has_access('Drug\DrugController@create') ? ['href' => '/drug/add', 'link_name' => '+ Add New'] : []
                            ],
                            'back' => false
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Brand Name</th>
                                        <th>Generic Name</th>
                                        <th>Dosage Form</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $counter = 1; ?>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{ $counter++ }}</td>
                                        <td>{{ $row->brand_name }}</td>
                                        <td>{{ $row->generic_name }}</td>
                                        <td>{{ $row->dosage_form }}</td>
                                        <td>
                                            {{ str_limit($row->description, 64) }}
                                            @if( strlen($row->description) > 64)
                                                <a class="label label-default" href="{{ url('drug/show', $row->drug_id) }}">more</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if(has_access('Drug\DrugController@edit'))
                                            <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit" href="{{url('drug/edit', $row->drug_id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                                            @endif

                                            @php if($row->is_active){$icn = 'glyphicon-minus'; $state = 'false'; $ttl = 'Deactivate'; $cls = 'btn-warning';}else{$icn = 'glyphicon-ok'; $state = 'true'; $ttl = 'Activate'; $cls = 'btn-info';} @endphp

                                            @if(has_access('Drug\DrugController@activate'))
                                            <a class="btn btn-xs btn-xs {{ $cls }}" data-toggle="tooltip" title="{{ $ttl }}" href="{{url('drug/activate/'.$row->drug_id.'/'.$state)}}"><span class="glyphicon {{ $icn }}"></span></a>
                                            @endif

                                            @if(has_access('Drug\DrugController@destroy'))
                                            <a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Remove" onclick="return confirm('Are you sure you want to remove this drug?')" href="{{url('drug/remove', $row->drug_id)}}"><span class="glyphicon glyphicon-trash"></span></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $rows->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection