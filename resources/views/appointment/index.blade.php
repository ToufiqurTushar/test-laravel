@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/appointment">Appointments</a></li>
                <li class="active">List</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $msg }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.search-criteria',[
                            'patient_name' => true,
                            'patient_mobile' => true,
                            'facilitator_name' => true,
                            'appointment_date' => true,
                            'cancel_state' => true
                        ])
                    </div>
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Appointments',
                            'badge' => $rows->total(),
                            'links' => [
                                has_access('Appointment\AppointmentController@canceled') ? ['href' => url('/appointment/canceled'), 'link_name' => 'Canceled'] : [],
                                has_access('Appointment\AppointmentController@checkedIn') ? ['href' => url('/appointment/checked-in'), 'link_name' => 'Checked-In'] : [],
                                has_access('Appointment\AppointmentController@create') ? ['href' => url('appointment/add'), 'link_name' => '+ Add New'] : []
                            ],
                            'back' => false
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Apmt. ID</th>
                                        <th>Patient</th>
                                        <th>Contact Number</th>
                                        <th>Appointment With</th>
                                        <th>Appointment Date</th>
                                        <th>Appointment Time</th>
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{$row->appointment_id}}</td>
                                        <td>{{$row->patient_name}}</td>
                                        <td>{{$row->patient_mobile}}</td>
                                        <td>{{$row->facilitator_name}}</td>
                                        <td><code data-toggle="tooltip" title="Appointment taken at: {{ $row->created_at ? date_to_day($row->created_at, true) : 'not available' }}">{{date_to_day($row->appointment_date)}}</code></td>
                                        <td><code>{{$row->appointment_time}}</code></td>
                                        <td>
                                            @if($row->is_canceled)
                                                <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Re-appointment" href="{{ url('appointment/add?pid='.$row->patient_id) }}"><span class="glyphicon glyphicon-calendar"></span></a>
                                                <span class="label label-default" data-toggle="tooltip" title="Canceled at: {{ date_to_day($row->canceled_at, true) }}"><span class="glyphicon glyphicon-remove"></span></span>
                                            @else
                                                @if($row->is_checked_in)
                                                    <span class="label label-default" data-toggle="tooltip" title="Checked-in at: {{ date_to_day($row->checked_in_at, true) }}"><span class="glyphicon glyphicon-check"></span></span>
                                                @else
                                                    @if($row->appointment_date < date('Y-m-d'))
                                                        <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Re-appointment" href="{{ url('appointment/add?pid='.$row->patient_id) }}"><span class="glyphicon glyphicon-calendar"></span></a>
                                                        <span class="label label-default" data-toggle="tooltip" title="Missed Appointment"><span class="glyphicon glyphicon-question-sign"></span></span>
                                                    @else
                                                        @if(has_access('Appointment\AppointmentController@checkIn'))
                                                            <a class="btn btn-xs btn-info" data-toggle="tooltip" title="Check-in" href="{{ url('appointment/check-in/'.$row->appointment_id.'/true') }}"><span class="glyphicon glyphicon-check"></span></a>
                                                        @endif
                                                        @if(has_access('Appointment\AppointmentController@edit'))
                                                            <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit" href="{{url('appointment/edit', $row->appointment_id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                                                        @endif
                                                        @if(has_access('Appointment\AppointmentController@activate'))
                                                            <a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Cancel" onclick="return confirm('Are you sure you want to cancel this appointment?')" href="{{url('appointment/cancel/'. $row->appointment_id .'/true')}}"><span class="glyphicon glyphicon-remove"></span></a>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $rows->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
@endsection

@section('script')
    <script src="{{ asset('lib/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function () {
            $('.date').datepicker({
                format:'yyyy-mm-dd',
                todayHighlight: true,
                clearBtn:true,
                todayBtn:true,
                autoclose:true,
                daysOfWeekHighlighted:"5,6"
            });

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
