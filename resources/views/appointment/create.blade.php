@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/appointment">Appointments</a></li>
                <li class="active">Add</li>
            </ol>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'New Appointment',
                            'links' => [['href' => '/appointment', 'link_name' => 'Appointments']],
                            'back' => true
                        ])
                    </div>

                    <div class="panel-body">
                        <div class="col-md-10">
                            @include('shared.errors')

                            <form class="form-horizontal" method="POST" action="{{ url('appointment') }}">
                                {{ csrf_field() }}

                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="apmt_date">Appointment Date & Time<span class="text-danger">*</span></label>
                                        <div class="col-md-4">
                                            <div class="input-group date">
                                                <input type="text" class="form-control" id="apmt_date" name="appointment_date" placeholder="choose date" value="{{ old('appointment_date') }}" required>
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group clockpicker">
                                                <input type="text" class="form-control" name="appointment_time" placeholder="choose time" value="{{ old('appointment_time') }}" required>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="facilitator_id">Appointment With<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            @inject('facilitator','App\Repositories\UserRepository')
                                            <select name="facilitator_id" id="facilitator_id" class="form-control" required>
                                                <option value="">----</option>
                                                {!! $facilitator->getAllAsOption(old('facilitator_id')) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="patient_id">Patient<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            @inject('patient','App\Repositories\PatientRepository')
                                            <select name="patient_id" id="patient_id" class="form-control" required>
                                                <option value="">----</option>
                                                {!! $patient->getAllAsOption(old('patient_id', $patient_id)) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="description">Description of Illness</label>
                                        <div class="col-md-8">
                                            <textarea rows="3" cols="7" class="form-control" id="description" placeholder="brief description of illness..." name="description">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                            <div class="modal" id="myModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Service Providers Schedule</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control input-sm" id="apmt_date" name="appointment_date" placeholder="choose date" value="{{ old('appointment_date') }}" required>
                                                            <div class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="facilitator_id" id="facilitator_id" class="form-control selector">
                                                            <option value="">facilitator</option>
                                                            {!! $facilitator->getAllAsOption() !!}
                                                        </select>
                                                    </div>
                                                    <div class="form-group pull-right">
                                                        <button type="button" class="btn btn-default btn-sm" id="show">Show</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 response">
                                                    <div class="text-info">Select appointment date and Service provider's name</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info btn-sm" id="viewSchedule">View Schedule</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/clockpicker.css') }}">
@endsection

@section('script')
    <script src="{{ asset('lib/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('lib/clockpicker.js') }}"></script>
    <script>
        $(function(){
            $('.date').datepicker({
                format:'yyyy-mm-dd',
                todayHighlight: true,
                clearBtn:true,
                todayBtn:true,
                autoclose:true,
                daysOfWeekHighlighted:"5,6",
                startDate:'today'
            });

            $('.clockpicker').clockpicker({
                placement: 'bottom',
                align: 'left',
                autoclose: true,
                donetext: 'Done'
            });

            $("#viewSchedule").click(function(){
                $("#myModal").modal().find('#show').click(function () {
                    var m = $('.modal-body').find('input,select');
                    var f = $(m).filter(function () {return $(this).val() === ''});
                    if($(f).length > 0){
                        $('.response').empty();
                        $(f).each(function () {
                            $('.response').append("<div class='alert alert-danger'>"+ $(this).attr('name') +" is empty!</div>")
                        });
                    } else {
                        $.ajax({
                            type: 'GET',
                            dataType: 'html',
                            url: "{{ url('appointment/show/times') }}",
                            data: $(m).serialize(),
                            success: function (response) {
                                $('.response').empty().append(response).promise().done(function () {
                                    $(this).find('.h5').html($(m).find('option:selected').text());
                                });
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                var msg = $('<div class="alert alert-danger"></div>');
                                if (jqXHR.status === 0) {
                                    $(msg).html('The request could not be sent!');
                                } else if (jqXHR.status == 404) {
                                    $(msg).html('Requested page not found. [404]!');
                                } else if (jqXHR.status == 500) {
                                    $(msg).html('Internal Server Error [500]!');
                                } else if (textStatus === 'timeout') {
                                    $(msg).html('Request timed out!');
                                } else {
                                    $(msg).html('Uncaught Error: ' + errorThrown);
                                }
                                $('.response').empty().append($(msg));
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection