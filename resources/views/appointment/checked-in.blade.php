@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/appointment">Appointments</a></li>
                <li class="active">Checked-in</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success" role="alert">{{ $msg }}</div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading', [
                            'form_title' => 'Checked-in Appointments',
                            'links' => [
                                has_access('Appointment\AppointmentController@index') ? ['href' => url('appointment'), 'link_name' => 'Appointments'] : []
                            ],
                            'back' => false
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Patient ID</th>
                                        <th>Patient</th>
                                        <th>Contact Number</th>
                                        <th>Appointment With</th>
                                        <th>Appointment Date & Time</th>
                                        <th>Check-in Time</th>
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($rows as $row)
                                    <tr>
                                        <td>{{ $row->patient_id }}</td>
                                        <td>{{ $row->patient_name }}</td>
                                        <td>{{ $row->patient_mobile }}</td>
                                        <td>{{ $row->facilitator_name }}</td>
                                        <td><code>{{ date_to_day($row->appointment_date).' @ '.$row->appointment_time }}</code></td>
                                        <td><code>{{ date_to_day($row->checked_in_at, true) }}</code></td>
                                        <td>
                                            {{--@if(has_access('Appointment\AppointmentController@manage'))--}}
                                            <a class="btn btn-xs btn-primary" href="{{url('appointment/manage', $row->appointment_id)}}">Manage</a>
                                            {{--@endif--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
