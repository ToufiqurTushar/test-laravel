@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="/home">Home</a></li>
                <li><a href="/investigation">Investigations</a></li>
                <li class="active">Show</li>
            </ol>

            @if($msg = session('msg'))
                <div class="alert alert-success" role="alert">{{ $msg }}</div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @include('shared.form-heading',[
                            'form_title'=>'Investigation Details',
                            'links' => [
                                has_access('Investigation\InvestigationController@edit') ? ['href' => url('investigation/edit', $row->investigation_id), 'link_name' => 'Edit'] : []
                            ],
                            'back' => true
                        ])
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <legend>{{ $row->name }}</legend>
                                <dl class="dl-horizontal">
                                    <dt>Investigation Name</dt>
                                    <dd>{{ $row->name }}</dd>
                                    <dt>Description</dt>
                                    <dd>{{ $row->description }}</dd>
                                    <dt>Result Parameters</dt>
                                    <dd>
                                        @foreach($row->params as $param)
                                            <span class="label label-success">{{ $param }}</span>
                                        @endforeach
                                    </dd>
                                </dl>
                                <a class="btn btn-primary btn-xs pull-right" href="{{ url('investigation') }}"><span class="glyphicon glyphicon-ok"></span> Okay</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('link')
    <link rel="stylesheet" href="{{ asset('css/ncd-style.css') }}">
@endsection