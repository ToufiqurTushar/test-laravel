import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export const store = new Vuex.Store({
    state:{
        info:{
            past_illness:[],
            personal:[],
            routine:[],
            meds:[],
            tests:[],
            page:'Present Complaints'
        },
        todo:[]
    },
    getters:{
        info(state){
            return state.info;
        },
        count(state){
            return state.info.length;
        }
    },
    mutations:{
        add(state, payload){
            state.todo.push({task:payload.todo.task,done:'no'});
        }
    },
    actions:{
        add(context, payload){
            setTimeout(()=>{
                context.commit(payload);
            },1000);
        }
    }
});