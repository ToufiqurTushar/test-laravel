
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import BloodPressure from "./components/BloodPressure";

require('./bootstrap');

window.Vue = require('vue');
window.dateFormat = require('dateformat');

import {store} from './store';
import VueRouter from 'vue-router';
import ClinicalManagement from './components/ClinicalManagement';
import PatientHistory from './components/PatientHistory';
import GeneralExamination from './components/GeneralExamination';
import SystemicExamination from './components/SystemicExamination';
import Diagnosis from './components/Diagnosis';
import Investigations from './components/Investigations';
import Medication from './components/Medication';
import Visit from './components/Visit';
import Prescription from './components/Prescription'

Vue.use(VueRouter);

Vue.mixin({
    filters:{
        dateFormat(val){
            return dateFormat(val, "mediumDate");
        }
    }
});

if(document.getElementById('clinicalManagement')){
    const routes = [
        { path: '/', component: PatientHistory},
        { path: '/examination', component: GeneralExamination },
        { path: '/systemic-examination', component: SystemicExamination},
        { path: '/diagnosis', component: Diagnosis },
        { path: '/investigations', component: Investigations },
        { path: '/blood-pressure', component: BloodPressure },
        { path: '/medication', component: Medication },
        { path: '/visit', component: Visit }
    ];

    const router = new VueRouter({
        routes // short for `routes: routes`
    });

    new Vue({
        el:'#clinicalManagement',
        router,
        data(){
            return {'page_title':'----'}
        },
        components:{ClinicalManagement},
        store
    });
}

if(document.getElementById('prescription')){
    const routes = [
        { path: '/', component: Visit},
        { path: '/visit', component: Visit }
    ];

    const router = new VueRouter({
        routes // short for `routes: routes`
    });

    new Vue({
        el:'#prescription',
        router,
        data(){
            return {'page_title':'----'}
        },
        components:{Prescription},
        store
    });
}
