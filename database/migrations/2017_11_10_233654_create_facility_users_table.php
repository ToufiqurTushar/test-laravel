<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_users', function (Blueprint $table){
            $table->increments('facility_user_id');
            $table->unsignedInteger('facility_id')->comment('id of clinic facility');
            $table->unsignedInteger('user_id')->comment('id of clinic facility');
            $table->date('assign_date')->nullable()->comment('when the user was assigned to this facility');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_users');
    }
}
