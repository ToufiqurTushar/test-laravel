<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->increments('drug_id');
            $table->string('brand_name', 128)->comment('common brand name of the drug given by pharmaceutical for marketing');
            $table->string('generic_name', 128)->comment('the generic name of the drug');
            $table->enum('dosage_form', ['Tablet','Capsule','Injection','Syrup'])->comment('the form of dosage like table/capsule/syrup');
            $table->enum('dosage_unit', ['mg','ml'])->comment('the unit of a dose like mg/ml etc...');
            $table->enum('age_group', ['Adult','Children'])->comment('the age group for which this drug is applicable');
            $table->enum('administration_route', ['Oral','Sublingual','Buccal','Rectal'])->comment('the channels of taking this drug');
            $table->text('description')->nullable()->comment('short description on the drug');
            $table->text('indication')->nullable()->comment('disease indication for usage');
            $table->string('photo', 64)->nullable()->comment('photograph of the drug');
            $table->string('is_active')->default(true)->comment('is the drug is allowed to be prescribed, default is "yes"');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs');
    }
}
