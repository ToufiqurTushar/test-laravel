<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('appointment_id');
            $table->integer('patient_id')->comment('for which patient is this appointment?');
            $table->integer('facilitator_id')->comment('whom the appointment with?');
            $table->date('appointment_date')->comment('date of appointment');
            $table->time('appointment_time')->comment('time of appointment');
            $table->text('description')->nullable()->comment('reason of the appointment in brief');
            $table->boolean('is_checked_in')->default(false)->comments('determines if the patient showed up to meet the doctor');
            $table->dateTime('checked_in_at')->nullable()->comments('date-time of checking in');
            $table->boolean('is_canceled')->default(false)->comments('determines if the appointment was canceled');
            $table->dateTime('canceled_at')->nullable()->comments('date-time of cancellation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
