<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicalManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinical_managements', function (Blueprint $table) {
            $table->increments('clinical_management_id');
            $table->date('visit_date');
            $table->integer('patient_id')->unsigned();
            $table->integer('doctor_id')->unsigned();
            $table->text('past_illness')->nullable();
            $table->text('routine')->nullable();
            $table->text('present_complaints')->nullable();
            $table->text('medical_history')->nullable();
            $table->text('medication')->nullable();
            $table->text('personal')->nullable();
            $table->text('family')->nullable();
            $table->text('social')->nullable();
            $table->text('allergy')->nullable();
            $table->text('psychiatric_illness')->nullable();
            $table->text('appearance')->nullable();
            $table->string('anaemia', 50)->nullable();
            $table->string('blood_pressure', 50)->nullable();
            $table->string('venous_pressure', 50)->nullable();
            $table->string('jaundice', 50)->nullable();
            $table->string('temperature', 50)->nullable();
            $table->string('clubbing', 50)->nullable();
            $table->string('cyanosis', 50)->nullable();
            $table->string('pulse', 50)->nullable();
            $table->string('oedema', 50)->nullable();
            $table->string('height', 50)->nullable();
            $table->string('weight', 50)->nullable();
            $table->text('other_examination')->nullable();
            $table->text('cardiac_vascular_system')->nullable();
            $table->text('respiratory_system')->nullable();
            $table->text('elementary_system')->nullable();
            $table->text('musculoskeletal_system')->nullable();
            $table->text('investigation_specific')->nullable();
            $table->text('diagnosis')->nullable();
            $table->text('diagnosis_remarks')->nullable();
            $table->text('meds')->nullable();
            $table->text('tests')->nullable();
            $table->text('medication_advice')->nullable();
            $table->string('menstruation', 50);
            $table->string('menstruation_type', 20);
            $table->string('para', 50);
            $table->string('gravida', 50);
            $table->string('menarche', 50);
            $table->string('menopause', 20);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinical_managements');
    }
}
