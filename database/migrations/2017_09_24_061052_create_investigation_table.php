<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigations', function (Blueprint $table) {
            $table->increments('investigation_id');
            $table->string('name', 128)->comment('title of the medical investigation');
            $table->text('description')->nullable()->comment('brief description about the investigation');
            $table->text('params')->nullable()->comment('json encoded result parameters of this investigation');
            $table->boolean('is_active')->default(true)->comment('if the investigation is allowed to be prescribed in the facility');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigations');
    }
}
