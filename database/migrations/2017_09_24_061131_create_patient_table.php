<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('patient_id');
            $table->string('name', 128);
            $table->string('phone', 32)->nullable();
            $table->string('mobile', 32)->nullable();
            $table->string('email', 128)->nullable();
            $table->string('address', 256)->comment('physical address of the facility');
            $table->string('religion', 16);
            $table->string('civil_status', 16);
            $table->string('sex', 16);
            $table->date('dob')->nullable()->comment('date of birth of the patient');
            $table->string('birth_place', 64)->nullable()->comment('name of the place, where the patient was born');
            $table->string('blood_group', 8)->nullable();
            $table->string('national_id', 32)->nullable()->comment('national id of the patient');
            $table->string('insurance_company', 64)->nullable()->comment('id of the insurance company, if the patient has health insurance');
            $table->string('insurance_policy_number', 64)->nullable()->comment('health policy id number, if there is any');
            $table->string('photo', 64)->nullable()->comment('photograph of the patient');
            $table->boolean('is_active')->default(true)->comment('is the patient is active, default is "yes"');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
