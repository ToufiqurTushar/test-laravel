<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientBloodPressureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_blood_pressure', function (Blueprint $table) {
            $table->increments('patient_blood_pressure_id');
            $table->integer('patient_id')->unsigned();
            $table->double('systolic');
            $table->double('diastolic');
            $table->double('pulse');
            $table->dateTime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_blood_pressure');
    }
}
