<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->increments('facility_id');
            $table->string('name', 128);
            $table->string('contact_person', 128)->comment('name of the contact person, may be non-medical');
            $table->string('email', 64)->nullable();
            $table->string('mobile', 32)->nullable();
            $table->string('phone', 32)->nullable();
            $table->string('address', 256)->nullable()->comment('physical address of the facility');
            $table->string('specialization', 128)->comment('names of diseases for which the facility is special');
            $table->date('engagement_date')->nullable()->comment('the date of enrollment under the ncd project');
            $table->string('photo', 64)->nullable()->comment('photograph of the facility');
            $table->boolean('is_active')->default(true)->comment('is the facility is active in providing service under the ncd project, default is "yes"');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilities');
    }
}
