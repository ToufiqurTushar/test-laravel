<?php

use Illuminate\Database\Seeder;

class DiseasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('diseases')->insert([
            [
                'name'=>'Tuberculosis',
                'description'=>'Tuberculosis is also known as TB, a curable disease but many people still think that TB is not curable!!!',
                'is_active'=>true
            ],
            [
                'name'=>'HIV',
                'description'=>'Human Immunodeficiency Virus (HIV) known as HIV aids is a non curable disease!!! Only awareness and protective measurements can save from this!!!',
                'is_active'=>true
            ],
        ]);
    }
}
