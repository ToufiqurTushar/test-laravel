<?php

use Illuminate\Database\Seeder;

class DrugsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('drugs')->insert([
            [
                'brand_name'=>'Napa',
                'generic_name'=>'Paracetamol',
                'dosage_form'=>'Tablet',
                'dosage_unit'=>'mg',
                'age_group'=>'Adult',
                'administration_route'=>'Oral',
                'description'=>'Paracetamol is a fast acting and safe analgesic with marked antipyretic property. It is specially suitable for aptients who for any reason can not tolerate aspirin or other analgesics etc...',
                'indication'=>'All conditions requiring relief from apin and fever such as neuritis, neuralgia, headache, erache, toothache, pain due to rheumatic disorder, cold, influenza, dysmenorrhoea, post-vaccination pain and fever of children etc...',
                'photo'=>null,
                'is_active'=>true
            ],
            [
                'brand_name'=>'Ace',
                'generic_name'=>'Paracetamol',
                'dosage_form'=>'Syrup',
                'dosage_unit'=>'ml',
                'age_group'=>'Children',
                'administration_route'=>'Oral',
                'description'=>'Paracetamol is a fast acting and safe analgesic with marked antipyretic property. It is specially suitable for aptients who for any reason can not tolerate aspirin or other analgesics etc...',
                'indication'=>'All conditions requiring relief from apin and fever such as neuritis, neuralgia, headache, erache, toothache, pain due to rheumatic disorder, cold, influenza, dysmenorrhoea, post-vaccination pain and fever of children etc...',
                'photo'=>null,
                'is_active'=>true
            ]
        ]);
    }
}
