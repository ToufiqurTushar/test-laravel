<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(DesignationsTableSeeder::class);
        $this->call(DiseasesTableSeeder::class);
        $this->call(DrugsTableSeeder::class);
        $this->call(FacilitiesTableSeeder::class);
        $this->call(FacilityUsersTableSeeder::class);
        $this->call(InvestigationsTableSeeder::class);
        $this->call(PatientsTableSeeder::class);
        $this->call(AppointmentsTableSeeder::class);

        $this->call(RoleTableSeeder::class);
        $this->call(ResourceTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UserRoleTableSeeder::class);
    }
}
