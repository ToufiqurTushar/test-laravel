<?php

use Illuminate\Database\Seeder;

class FacilityUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facility_users')->insert([
            [
                'facility_id' => 1,
                'user_id' => 1
            ],
            [
                'facility_id' => 1,
                'user_id' => 2
            ],
            [
                'facility_id' => 2,
                'user_id' => 1
            ],
            [
                'facility_id' => 2,
                'user_id' => 2
            ]
        ]);
    }
}
