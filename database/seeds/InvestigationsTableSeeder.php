<?php

use Illuminate\Database\Seeder;

class InvestigationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('investigations')->insert([
            [
                'name'=>'Hepatitis B Virus Testing',
                'description'=>'Hepatitis B serologic testing involves measurement of several hepatitis B virus (HBV)-specific antigens and antibodies. Different serologic “markers” or combinations of markers are used to identify different phases of HBV infection and to determine whether a patient has acute or chronic HBV infection is immune to HBV as a result of prior infection or vaccination, or is susceptible to infection',
                'params'=>'HBsAg,anti-HBc,anti-HBs,IgM anti-HBc,HBeAg,HBV DNA',
                'is_active'=>true
            ],
            [
                'name'=>'CBC',
                'description'=>'CBC is a very important medical investigation of Blood. The full meaning of CBC is Complete Blood Count!!',
                'params'=>'Hemoglobin, RBC, WBC, Hct, MCV,Platelets',
                'is_active'=>true
            ],
        ]);
    }
}
