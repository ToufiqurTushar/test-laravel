<?php

use Illuminate\Database\Seeder;

class FacilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facilities')->insert([
            [
                'name'=>'Uttara Clinic',
                'contact_person'=>'Contact of Uttara Clinic',
                'email'=>'uttara@example.com',
                'mobile'=>'01736545637',
                'phone'=>'887464683',
                'address'=>'Physical address of Uttara clinic',
                'specialization'=>'Tuberculosis',
                'engagement_date'=>null,
                'photo'=>null,
                'is_active'=>true,
            ],
            [
                'name'=>'Jatrabari Clinic',
                'contact_person'=>'Contact of Jatrabari Clinic',
                'email'=>'jatrabari@example.com',
                'mobile'=>'01736545637',
                'phone'=>'887464683',
                'address'=>'Physical address of Jatrabari clinic',
                'specialization'=>'HIV',
                'engagement_date'=>null,
                'photo'=>null,
                'is_active'=>true,
            ],
        ]);
    }
}
