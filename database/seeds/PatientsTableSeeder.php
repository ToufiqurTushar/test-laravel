<?php

use Illuminate\Database\Seeder;

class PatientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patients')->insert([
            [
                'name'=>'Rahat Bashir',
                'phone'=>'880989284',
                'mobile'=>'8801715812079',
                'email'=>'rahat.bashir@yano.tech',
                'address'=>'Address of Rahat Bashir',
                'religion'=>'Islam',
                'civil_status'=>'Married',
                'sex'=>'Male',
                'dob'=>'1976-09-03',
                'birth_place'=>'Dhaka',
                'blood_group'=>'B+',
                'national_id'=>'938294729387979223423',
                'insurance_company'=>null,
                'insurance_policy_number'=>null,
                'photo'=>null,
                'is_active'=>true,
            ],
            [
                'name'=>'Mahabubul Hasan',
                'phone'=>'880989284',
                'mobile'=>'8801715812079',
                'email'=>'mahabubul@yano.tech',
                'address'=>'Address of Rahat Bashir',
                'religion'=>'Islam',
                'civil_status'=>'Single',
                'sex'=>'Male',
                'dob'=>'1976-09-03',
                'birth_place'=>'Dhaka',
                'blood_group'=>'B+',
                'national_id'=>'938294729387979223423',
                'insurance_company'=>null,
                'insurance_policy_number'=>null,
                'photo'=>null,
                'is_active'=>true,
            ],
        ]);
    }
}
