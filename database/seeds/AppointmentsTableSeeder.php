<?php

use Illuminate\Database\Seeder;

class AppointmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appointments')->insert([
            [
                'patient_id' => 1
                , 'facilitator_id' => 1
                , 'appointment_date' => '2017-10-25'
                , 'appointment_time' => '16:30:00'
                , 'description' => 'cold, temperature, and pain in whole body'
                , 'is_checked_in' => false
                , 'checked_in_at' => null
                , 'is_canceled' => false
                , 'canceled_at' => null
            ],
            [
                'patient_id' => 2
                , 'facilitator_id' => 1
                , 'appointment_date' => '2017-10-25'
                , 'appointment_time' => '17:30:00'
                , 'description' => 'cold, temperature, and pain in whole body'
                , 'is_checked_in' => false
                , 'checked_in_at' => null
                , 'is_canceled' => false
                , 'canceled_at' => null
            ]
        ]);
    }
}
