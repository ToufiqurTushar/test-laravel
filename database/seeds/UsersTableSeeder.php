<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'name'=>'Rahat Bashir',
            'email'=>'rahat.bashir@yano.tech',
            'password'=>bcrypt('123456')
            ],
            [
            'name'=>'Mahabubul Hasan',
            'email'=>'mahabub@yano.tech',
            'password'=>bcrypt('123456')
            ],
        ]);
    }
}
